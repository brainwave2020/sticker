'''
frame type
'''


class FrameType:
    frametype = 0
    img_num = 0

    def set_frame(self, type):
        self.frametype = type

    def get_frame(self):
        return self.frametype

    def set_img_num(self):
        if self.frametype in [1, 2]:
            self.img_num = 3
        elif self.frametype in [3, 4]:
            self.img_num = 4

    def get_img_num(self):
        return self.img_num
