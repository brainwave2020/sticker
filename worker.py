import sys

from PyQt6.QtCore import QObject, QThread, pyqtSignal, pyqtSlot
from PyQt6.QtWidgets import QApplication, QPushButton, QTextEdit, QVBoxLayout, QWidget

import time
import requests


def trap_exc_during_debug(*args):
    print(args)


sys.excepthook = trap_exc_during_debug


class Worker(QObject):
    url_checkstatus = 'https://sandbox-appsrv2.chillpay.co/api/v2/PaymentStatus/'
    payload_headers = {'Content-Type': 'application/x-www-form-urlencoded'}

    # signal_start = pyqtSignal(int)
    # signal_stop = pyqtSignal(int)
    # signal_msg = pyqtSignal(str)
    finished = pyqtSignal()
    progress = pyqtSignal(int)

    def work(self, payload_payment):
        # keep calling
        # res = requests.post(url=self.url_checkstatus, data=payload_payment, headers=self.payload_headers)
        # data = res.json()
        # print(data)
        for i in range(5):
            time.sleep(1)
            self.progress.emit(i + 1)

    # def abort(self):
    #     self.signal_msg.emit('stop')
    #     self.__abort = True


if __name__ == "__main__":
    app = QApplication([])
    sys.exit(app.exec())
