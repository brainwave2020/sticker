# -*- mode: python ; coding: utf-8 -*-


block_cipher = None
bundle_files = [('UI/icon/icon256.ico','UI/icon')]

a = Analysis(['window_main.py'],
             pathex=[],
             binaries=[],
             datas=bundle_files,
             hiddenimports=[],
             hookspath=['.'],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          name='FOTOMACHINE',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=False,
          runtime_tmpdir=None,
          console=False,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None,
          icon='ico/icon256.ico')

COLLECT(exe,
        a.binaries,
        a.zipfiles,
        a.datas,
        strip=False,
        upx=False,
        name='FOTOMACHINE',
        icon='ico/icon256.ico')