import tkinter as tk
from tkinter import messagebox as msgbox
import time

import os

import requests
import hashlib
import datetime

import json

from cv2 import cv2
from PIL import Image, ImageTk
import imutils

import win32ui
import win32print
from PIL import ImageWin
import qrcode

from cefpython3 import cefpython as cef
import ctypes
import sys
import platform
import logging as _logging

WindowUtils = cef.WindowUtils()
WINDOWS = (platform.system() == "Windows")
logger = _logging.getLogger("tkinter_.py")

config = dict()
with open("config.json") as config_file:
    config.update(json.load(config_file))
config_file.close()

########################################################################
class Payment(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.configure(bg='black')
        self.title("QR Payment")

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./UI/bg/10 Loading.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        # self.url_makepayment = 'https://appsrv.chillpay.co/api/v2/Payment/'
        # self.url_checkstatus = 'https://appsrv.chillpay.co/api/v2/PaymentStatus/'
        # self.cpSKEY = '2nN0JQZpktNC44MjYQzeo4ZKzWCVjCv5FypHRuI2T9jD8nU4EwtVPYmRXZ2XF7YT1Jhpq7nbGQ3kc8buVjITDpYbnE9EKqaB4tVrns1RgSVSnBThsRXGGvzdGcdLSY7KX3dxIC2cSdMY8lnzrItYbNylVTLL9CqAXxvNd'
        # self.APIKey = 'KeV0hOq8z20Uhh61maHmpvJ1AWu0kVZ87CXrXDJNhCnizKzvfOHaG616E90u6pj2'
        # self.payload_headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        # self.payload_payment = {
        #     'MerchantCode': 'M032891',
        #     'OrderNo': '00001',
        #     'CustomerId': config.get('machine')['mID'],
        #     'Amount': '2000',
        #     'ChannelCode': 'bank_qrcode',
        #     'Currency': '764',
        #     'RouteNo': '1',
        #     'IPAddress': '127.0.0.1',
        #     'APIKey': 'KeV0hOq8z20Uhh61maHmpvJ1AWu0kVZ87CXrXDJNhCnizKzvfOHaG616E90u6pj2',
        #     'Checksum': '',
        # }
        # self.payload_result = dict()
        # self.payload_paymentstatus = {
        #     'MerchantCode': 'M032891',
        #     'TransactionId': '',
        #     'APIKey': 'KeV0hOq8z20Uhh61maHmpvJ1AWu0kVZ87CXrXDJNhCnizKzvfOHaG616E90u6pj2',
        #     'Checksum': '',
        # }

        self.url_makepayment = 'https://sandbox-appsrv2.chillpay.co/api/v2/Payment/'
        self.url_checkstatus = 'https://sandbox-appsrv2.chillpay.co/api/v2/PaymentStatus/'
        self.cpSKEY = 'tqtBZkzw1GHJvLVe3FPdoWNqAwe3a3369IYMYMYuD6z4dKzs36VawdBye6IzI7d5rcn6OGdOqnQN3gNlfmyjA9WWMJzYrTItJ8uR416BRWvYb8WXUmqAsBymt1QJdCK3EqG6QDo6YVOCEiWpsL0ZnzqkxI3GAT1Ty4opx'
        self.APIKey = 'HFDFNVSgdMuLYdL2Kt8ZE6DfZTcLHPLwY5wD9QLp2xRjxnAOuglP6VVa5gQcnLWo'
        self.payload_headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        self.payload_payment = {
            'MerchantCode': 'M032829',
            'OrderNo': '00001',
            'CustomerId': config.get('machine')['mID'],
            'Amount': '2000',
            'ChannelCode': 'bank_qrcode',
            'Currency': '764',
            'RouteNo': '1',
            'IPAddress': '127.0.0.1',
            'APIKey': 'HFDFNVSgdMuLYdL2Kt8ZE6DfZTcLHPLwY5wD9QLp2xRjxnAOuglP6VVa5gQcnLWo',
            'Checksum': '',
        }
        self.payload_result = dict()
        self.payload_paymentstatus = {
            'MerchantCode': 'M032829',
            'TransactionId': '',
            'APIKey': 'HFDFNVSgdMuLYdL2Kt8ZE6DfZTcLHPLwY5wD9QLp2xRjxnAOuglP6VVa5gQcnLWo',
            'Checksum': '',
        }

        btn1 = Image.open('./UI/button/B3.png')
        btn1 = btn1.resize((470, 180), Image.ANTIALIAS)
        btn1 = ImageTk.PhotoImage(image=btn1)
        self.bgcanvas.btn1 = btn1
        btn1action = self.bgcanvas.create_image(70, 1670, image=btn1, anchor='nw')
        self.bgcanvas.tag_bind(btn1action, '<Button-1>', lambda x: self.to_main())

        self.webframe = tk.Frame(self)
        self.webframe.place(anchor='nw', x=75, y=75, width=930, height=1300)
        self.webframe.configure(bg='white')

        self.brframe = BrowserFrame(self.webframe)
        self.brframe.pack(expand=True, fill='both')

        self.resStat = 9
        self.countdown = 88
        self.payment_makepayment()

    def making_webview(self):
        self.brframe.set_url(self.payload_result.get('PaymentUrl'))
        self.after(3000, self.payment_status_keepcalling)

    def payment_makepayment(self):
        s2h = self.prepares2h(self.payload_payment)
        hhStr = self.hashStr(s2h)
        self.payload_payment.update(Checksum=hhStr)

        # requests payment
        res = requests.post(url=self.url_makepayment, data=self.payload_payment, headers=self.payload_headers)
        data = res.json()

        if data.get('Status') == 0:
            self.payload_result.update(data)
            self.payload_paymentstatus.update(TransactionId=data.get('TransactionId'))
            # prepare payload status
            s2h = self.prepares2h(self.payload_paymentstatus)
            hhStr = self.hashStr(s2h)
            self.payload_paymentstatus.update(Checksum=hhStr)
            self.making_webview()
        else:
            msgbox.showerror(title='FAILED', message='make requests fail')
            self.to_main()

    def payment_status_keepcalling(self):
        res = requests.post(url=self.url_checkstatus, data=self.payload_paymentstatus, headers=self.payload_headers)
        resRaw = res.json()
        resStat = resRaw.get('PaymentStatus')
        self.resStat = resStat
        # print('keepcall return status', resStat)
        # print(self.countdown)

        # time.sleep(1)
        self.countdown -= 1

        if self.countdown > 1:
            if self.resStat == 9:
                self.after(1000, self.payment_status_keepcalling)
                # self.payment_status_keepcalling()
            else:
                if self.resStat == 0:
                    # self.QRwindow.destroy()
                    # cef.Shutdown()
                    # print('Payment successed : status {}'.format(resStat))
                    self.to_frame()
                elif self.resStat in [1, 2, 3]:
                    # self.QRwindow.destroy()
                    # cef.Shutdown()
                    msgbox.showerror(title='ERROR', message='Payment failed or canceled.')
                    # print('Payment failed : status {}'.format(resStat))
                    self.to_main()
                else:
                    # self.QRwindow.destroy()
                    # cef.Shutdown()
                    msgbox.showerror(title='ERROR', message='ERROR.')
                    # print('Payment failed : status {}'.format(resStat))
                    self.to_main()
        else:
            # self.QRwindow.destroy()
            # cef.Shutdown()
            self.to_main()

    def prepares2h(self, data):
        s2h = ''
        for i in data:
            s2h += str(data.get(i))
        s2h += self.cpSKEY
        return s2h

    def hashStr(self, s2h):
        hhStr = hashlib.md5(str(s2h).encode('utf-8')).hexdigest()
        return hhStr

    def to_main(self):
        self.after(400, self.destroy)
        self.original_frame.show()

    def to_frame(self):
        self.after(400, self.destroy)
        if config.get('screen').get('frame_screen') == 1:
            subFrame = FrameSelect_1(self.original_frame)
        elif config.get('screen').get('frame_screen') == 2:
            subFrame = FrameSelect_2(self.original_frame)
        elif config.get('screen').get('frame_screen') == 3:
            subFrame = FrameSelect_3(self.original_frame)
        elif config.get('screen').get('frame_screen') == 4:
            subFrame = FrameSelect_4(self.original_frame)
        else:
            subFrame = FrameSelect_1(self.original_frame)


class BrowserFrame(tk.Frame):

    def __init__(self, mainframe, navigation_bar=None):
        self.navigation_bar = navigation_bar
        self.closing = False
        self.browser = None
        tk.Frame.__init__(self, mainframe)
        self.mainframe = mainframe
        self.bind("<FocusIn>", self.on_focus_in)
        self.bind("<FocusOut>", self.on_focus_out)
        self.bind("<Configure>", self.on_configure)
        self.payurl = ''
        self.focus_set()

    def set_url(self, newurl):
        self.payurl = newurl

    def embed_browser(self):
        # print(self.payurl)
        window_info = cef.WindowInfo()
        rect = [0, 0, self.winfo_width(), self.winfo_height()]
        window_info.SetAsChild(self.get_window_handle(), rect)
        self.browser = cef.CreateBrowserSync(window_info, url=self.payurl)
        assert self.browser
        # self.browser.SetClientHandler(LifespanHandler(self))
        # self.browser.SetClientHandler(LoadHandler(self))
        # self.browser.SetClientHandler(FocusHandler(self))
        self.message_loop_work()

    def get_window_handle(self):
        if self.winfo_id() > 0:
            return self.winfo_id()
        else:
            raise Exception("Couldn't obtain window handle")

    def message_loop_work(self):
        cef.MessageLoopWork()
        self.after(10, self.message_loop_work)

    def on_configure(self, _):
        if not self.browser:
            self.embed_browser()

    def on_root_configure(self):
        if self.browser:
            self.browser.NotifyMoveOrResizeStarted()

    def on_mainframe_configure(self, width, height):
        if self.browser:
            if WINDOWS:
                ctypes.windll.user32.SetWindowPos(
                    self.browser.GetWindowHandle(), 0,
                    0, 0, width, height, 0x0002)
            self.browser.NotifyMoveOrResizeStarted()

    def on_focus_in(self, _):
        # logger.debug("BrowserFrame.on_focus_in")
        if self.browser:
            self.browser.SetFocus(True)

    def on_focus_out(self, _):
        logger.debug("BrowserFrame.on_focus_out")

    def on_root_close(self):
        # logger.info("BrowserFrame.on_root_close")
        if self.browser:
            # logger.debug("CloseBrowser")
            self.browser.CloseBrowser(True)
            self.clear_browser_references()
        else:
            # logger.debug("tk.Frame.destroy")
            self.destroy()

    def clear_browser_references(self):
        # Clear browser references that you keep anywhere in your
        # code. All references must be cleared for CEF to shutdown cleanly.
        self.browser = None


########### demo
# class Demo(tk.Toplevel):
#     def __init__(self, original):
#         self.original_frame = original
#         tk.Toplevel.__init__(self)
#         self.geometry("400x711")
#         self.title("Coupon")
#
#         btn = tk.Button(self, text="Main Menu", command=self.to_main)
#         btn.pack()
#
#     def to_main(self):
#         self.destroy()
#         self.original_frame.show()
############

class Coupon(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.attributes('-topmost', 'true')
        self.title("Voucher")
        self.configure(bg='black')

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./UI/bg/4.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        btn1 = Image.open('./UI/button/B3.png')
        btn1 = btn1.resize((470, 180), Image.ANTIALIAS)
        btn1 = ImageTk.PhotoImage(image=btn1)
        self.bgcanvas.btn1 = btn1
        btn1action = self.bgcanvas.create_image(70, 1670, image=btn1, anchor='nw')
        self.bgcanvas.tag_bind(btn1action, '<Button-1>', lambda x: self.to_main())

        # self.panel2 = tk.Label(self)
        # self.panel.place(anchor='nw', x=10, y=10, width=200, height=200)

        # self.vs = cv2.VideoCapture(0)
        self.vs = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        self.qrCodeDetector = cv2.QRCodeDetector()
        self.current_image = None
        self.img_scan = None

        self.panel = tk.Label(self)
        # self.panel.pack(padx=10, pady=10)
        self.panel.place(anchor='nw', x=270, y=480, width=540, height=540)

        # btn = tk.Button(self, text="Main Menu", command=self.to_main)
        # btn.pack()
        self.couponcode = ''

        self.video_loop()
        self.qrreader()

    def video_loop(self):
        ok, frame = self.vs.read()
        if ok:
            cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            cv2image = cv2.flip(cv2image, 1)
            cv2image = imutils.resize(cv2image, width=600)
            self.current_image = Image.fromarray(cv2image)
            self.img_scan = frame
            imgtk = ImageTk.PhotoImage(image=self.current_image)
            self.panel.imgtk = imgtk
            self.panel.config(image=imgtk)
        self.after(40, self.video_loop)

    def destructor(self):
        # print("[INFO] qr reader closing...")
        self.vs.release()
        cv2.destroyAllWindows()

    def qrreader(self):
        data, points, _ = self.qrCodeDetector.detectAndDecode(self.img_scan)
        # print('scanning')
        if data:
            # print('QR founded')
            self.couponcode = data
            self.check_coupon()
        else:
            self.after(1000, self.qrreader)

    def check_coupon(self):
        self.vs.release()
        # request
        # print(payload)
        # res = requests.post(url='', data=payload)
        # res = res.json()
        url = 'https://fotomachine.co/API/barcode/redeem'
        reqpaylod = {
            'machine_id': config['machine']['mID'],
            'type': 'coupon',
            'code': self.couponcode
        }
        # payload = json.dumps(reqpaylod)
        payload = reqpaylod
        # print(type(payload))
        # print(reqpaylod)
        res = requests.post(url, data=payload, headers={'Content-Type': 'application/x-www-form-urlencoded'})
        data = res.json()
        # print(data)

        if data['success'] == 'success':
            self.to_frame()
        else:
            msgbox.showerror(title='FAILED', message=data['message_th'])
            self.to_main()

        # if self.couponcode == 'this is test code':
        #     self.to_frame()
        # else:
        #     msgbox.showerror(title='FAILED', message='QR code invalid!')
        #     self.to_main()
        # if success : to_frame

    def to_main(self):
        self.destructor()
        self.after(400, self.destroy)
        self.original_frame.show()

    def to_frame(self):
        self.destructor()
        self.after(400, self.destroy)
        if config.get('screen').get('frame_screen') == 1:
            subFrame = FrameSelect_1(self.original_frame)
        elif config.get('screen').get('frame_screen') == 2:
            subFrame = FrameSelect_2(self.original_frame)
        elif config.get('screen').get('frame_screen') == 3:
            subFrame = FrameSelect_3(self.original_frame)
        elif config.get('screen').get('frame_screen') == 4:
            subFrame = FrameSelect_4(self.original_frame)
        else:
            subFrame = FrameSelect_1(self.original_frame)


# singleton for frametype
def singleton(cls, *args, **kw):
    instances = {}

    def _singleton(*args, **kw):
        if cls not in instances:
            instances[cls] = cls(*args, **kw)
        return instances[cls]

    return _singleton


@singleton
class FrameType:
    frame = dict()
    layout = dict()

    layoutlist2 = {
        0: {'shot': 4, 'col': 2, 'row': 2, 'reso': [800, 800], 'mt': 0, 'mode': 'n', 'cut': True},
        1: {'shot': 4, 'col': 2, 'row': 2, 'reso': [800, 800], 'mt': 0, 'mode': 'n', 'cut': False},
        2: {'shot': 4, 'col': 2, 'row': 2, 'reso': [1080, 800], 'mt': 5, 'mode': 'n', 'cut': False},
        3: {'shot': 3, 'col': 2, 'row': 3, 'reso': [720, 800], 'mt': 5, 'mode': 'd', 'cut': True},
        4: {'shot': 6, 'col': 2, 'row': 3, 'reso': [720, 800], 'mt': 5, 'mode': 'n', 'cut': True},
        5: {'shot': 4, 'col': 2, 'row': 4, 'reso': [540, 800], 'mt': 5, 'mode': 'd', 'cut': True},
        6: {'shot': 8, 'col': 2, 'row': 4, 'reso': [540, 800], 'mt': 5, 'mode': 'n', 'cut': True},
    }

    layoutlist = {
        0: {'shot': 4, 'col': 2, 'row': 2, 'reso': [1080, 1080], 'fwhite': 3240, 'mt': 0, 'mode': 'n', 'cut': True},
        1: {'shot': 4, 'col': 2, 'row': 2, 'reso': [1080, 1080], 'fwhite': 3240, 'mt': 0, 'mode': 'n', 'cut': False},
        2: {'shot': 4, 'col': 2, 'row': 2, 'reso': [1080, 800], 'fwhite': 2400, 'mt': 5, 'mode': 'n', 'cut': False},
        3: {'shot': 3, 'col': 2, 'row': 3, 'reso': [972, 1080], 'fwhite': 3240, 'mt': 5, 'mode': 'd', 'cut': True},
        4: {'shot': 6, 'col': 2, 'row': 3, 'reso': [972, 1080], 'fwhite': 3240, 'mt': 5, 'mode': 'n', 'cut': True},
        5: {'shot': 4, 'col': 2, 'row': 4, 'reso': [729, 1080], 'fwhite': 3240, 'mt': 5, 'mode': 'd', 'cut': True},
        6: {'shot': 8, 'col': 2, 'row': 4, 'reso': [729, 1080], 'fwhite': 3240, 'mt': 5, 'mode': 'n', 'cut': True},
    }

    framelist = {
        1: {
            1: {'path': './UI/frame/screen1/Frame1.png'},
            2: {'path': './UI/frame/screen1/Frame2.png'},
            3: {'path': './UI/frame/screen1/Frame3.png'},
            4: {'path': './UI/frame/screen1/Frame4.png'},
        },
        2: {
            1: {'path': './UI/frame/screen2/Frame1.png'},
            2: {'path': './UI/frame/screen2/Frame2.png'},
            3: {'path': './UI/frame/screen2/Frame3.png'},
            4: {'path': './UI/frame/screen2/Frame4.png'},
        },
        3: {
            1: {'path': './UI/frame/screen3/Frame1.png'},
            2: {'path': './UI/frame/screen3/Frame2.png'},
            3: {'path': './UI/frame/screen3/Frame3.png'},
            4: {'path': './UI/frame/screen3/Frame4.png'},
            5: {'path': './UI/frame/screen3/Frame5.png'},
            6: {'path': './UI/frame/screen3/Frame6.png'},
            7: {'path': './UI/frame/screen3/Frame7.png'},
        },
        4: {
            1: {'path': './UI/frame/screen4/Frame1.png'},
            2: {'path': './UI/frame/screen4/Frame2.png'},
            3: {'path': './UI/frame/screen4/Frame3.png'},
            4: {'path': './UI/frame/screen4/Frame4.png'},
            5: {'path': './UI/frame/screen4/Frame5.png'},
            6: {'path': './UI/frame/screen4/Frame6.png'},
            7: {'path': './UI/frame/screen4/Frame7.png'},
        }
    }

    def set_frame(self, screen, iframe):
        self.frame = self.framelist[screen][iframe]

    def set_layout(self, typ):
        self.layout.clear()
        self.layout.update(self.layoutlist[typ])

    def get_frame(self):
        return self.frame

    def get_layout(self):
        return self.layout


class FrameSelect_1(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.attributes('-topmost', 'true')
        self.title("Frame")
        self.configure(bg='black')

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./UI/bg/5.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        bFrame1 = Image.open('./UI/frame/screen1/F1.png')
        bFrame1 = bFrame1.resize((288, 432), Image.ANTIALIAS)
        bFrame1 = ImageTk.PhotoImage(image=bFrame1)

        bFrame2 = Image.open('./UI/frame/screen1/F2.png')
        bFrame2 = bFrame2.resize((288, 432), Image.ANTIALIAS)
        bFrame2 = ImageTk.PhotoImage(image=bFrame2)

        bFrame3 = Image.open('./UI/frame/screen1/F3.png')
        bFrame3 = bFrame3.resize((288, 432), Image.ANTIALIAS)
        bFrame3 = ImageTk.PhotoImage(image=bFrame3)

        bFrame4 = Image.open('./UI/frame/screen1/F4.png')
        bFrame4 = bFrame4.resize((288, 432), Image.ANTIALIAS)
        bFrame4 = ImageTk.PhotoImage(image=bFrame4)

        bFrame1c = Image.open('./UI/frame/screen1/F1-Click.png')
        bFrame1c = bFrame1c.resize((288, 432), Image.ANTIALIAS)
        bFrame1c = ImageTk.PhotoImage(image=bFrame1c)

        bFrame2c = Image.open('./UI/frame/screen1/F2-Click.png')
        bFrame2c = bFrame2c.resize((288, 432), Image.ANTIALIAS)
        bFrame2c = ImageTk.PhotoImage(image=bFrame2c)

        bFrame3c = Image.open('./UI/frame/screen1/F3-Click.png')
        bFrame3c = bFrame3c.resize((288, 432), Image.ANTIALIAS)
        bFrame3c = ImageTk.PhotoImage(image=bFrame3c)

        bFrame4c = Image.open('./UI/frame/screen1/F4-Click.png')
        bFrame4c = bFrame4c.resize((288, 432), Image.ANTIALIAS)
        bFrame4c = ImageTk.PhotoImage(image=bFrame4c)

        self.bFrame1 = bFrame1
        self.bFrame2 = bFrame2
        self.bFrame3 = bFrame3
        self.bFrame4 = bFrame4

        self.bFrame1c = bFrame1c
        self.bFrame2c = bFrame2c
        self.bFrame3c = bFrame3c
        self.bFrame4c = bFrame4c

        self.bgcanvas.bFrame1 = self.bFrame1
        self.bgcanvas.bFrame2 = self.bFrame2
        self.bgcanvas.bFrame3 = self.bFrame3
        self.bgcanvas.bFrame4 = self.bFrame4

        self.bgcanvas.bFrame1c = self.bFrame1c
        self.bgcanvas.bFrame2c = self.bFrame2c
        self.bgcanvas.bFrame3c = self.bFrame3c
        self.bgcanvas.bFrame4c = self.bFrame4c

        self.bFrame1action = self.bgcanvas.create_image(310, 528, image=self.bFrame1, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame1action, '<Button-1>', lambda x: self.click_frame(1, 3))
        self.bFrame2action = self.bgcanvas.create_image(643, 528, image=self.bFrame2, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame2action, '<Button-1>', lambda x: self.click_frame(2, 3))
        self.bFrame3action = self.bgcanvas.create_image(310, 960, image=self.bFrame3, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame3action, '<Button-1>', lambda x: self.click_frame(3, 2))
        self.bFrame4action = self.bgcanvas.create_image(643, 960, image=self.bFrame4, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame4action, '<Button-1>', lambda x: self.click_frame(4, 2))

    def click_frame(self, pos, fid):
        self.resetAllClick()
        frametype = FrameType()
        frametype.set_layout(fid)
        if pos == 1:
            self.bgcanvas.itemconfig(self.bFrame1action, image=self.bFrame1c)
            frametype.set_frame(1, 1)
        elif pos == 2:
            self.bgcanvas.itemconfig(self.bFrame2action, image=self.bFrame2c)
            frametype.set_frame(1, 2)
        elif pos == 3:
            self.bgcanvas.itemconfig(self.bFrame3action, image=self.bFrame3c)
            frametype.set_frame(1, 3)
        elif pos == 4:
            self.bgcanvas.itemconfig(self.bFrame4action, image=self.bFrame4c)
            frametype.set_frame(1, 4)

        # create NEXT button
        bNext = Image.open('./UI/Button/B4.png')
        bNext = bNext.resize((470, 180), Image.ANTIALIAS)
        bNext = ImageTk.PhotoImage(image=bNext)
        self.bgcanvas.bNext = bNext
        bNextaction = self.bgcanvas.create_image(540, 1670, image=bNext, anchor='nw')
        self.bgcanvas.tag_bind(bNextaction, '<Button-1>', lambda x: self.to_wait())

    def resetAllClick(self):
        self.bgcanvas.itemconfig(self.bFrame1action, image=self.bFrame1)
        self.bgcanvas.itemconfig(self.bFrame2action, image=self.bFrame2)
        self.bgcanvas.itemconfig(self.bFrame3action, image=self.bFrame3)
        self.bgcanvas.itemconfig(self.bFrame4action, image=self.bFrame4)

    def to_wait(self):
        self.after(400, self.destroy)
        subFrame = WaitingPage(self.original_frame)

    def to_main(self):
        self.after(400, self.destroy)
        self.original_frame.show()


class FrameSelect_2(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.attributes('-topmost', 'true')
        self.title("Frame")
        self.configure(bg='black')

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./UI/bg/6.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        bFrame1 = Image.open('./UI/frame/screen2/F1.png')
        bFrame1 = bFrame1.resize((288, 432), Image.ANTIALIAS)
        bFrame1 = ImageTk.PhotoImage(image=bFrame1)

        bFrame2 = Image.open('./UI/frame/screen2/F2.png')
        bFrame2 = bFrame2.resize((288, 432), Image.ANTIALIAS)
        bFrame2 = ImageTk.PhotoImage(image=bFrame2)

        bFrame3 = Image.open('./UI/frame/screen2/F3.png')
        bFrame3 = bFrame3.resize((288, 432), Image.ANTIALIAS)
        bFrame3 = ImageTk.PhotoImage(image=bFrame3)

        bFrame4 = Image.open('./UI/frame/screen2/F4.png')
        bFrame4 = bFrame4.resize((288, 432), Image.ANTIALIAS)
        bFrame4 = ImageTk.PhotoImage(image=bFrame4)

        bFrame1c = Image.open('./UI/frame/screen2/F1-Click.png')
        bFrame1c = bFrame1c.resize((288, 432), Image.ANTIALIAS)
        bFrame1c = ImageTk.PhotoImage(image=bFrame1c)

        bFrame2c = Image.open('./UI/frame/screen2/F2-Click.png')
        bFrame2c = bFrame2c.resize((288, 432), Image.ANTIALIAS)
        bFrame2c = ImageTk.PhotoImage(image=bFrame2c)

        bFrame3c = Image.open('./UI/frame/screen2/F3-Click.png')
        bFrame3c = bFrame3c.resize((288, 432), Image.ANTIALIAS)
        bFrame3c = ImageTk.PhotoImage(image=bFrame3c)

        bFrame4c = Image.open('./UI/frame/screen2/F4-Click.png')
        bFrame4c = bFrame4c.resize((288, 432), Image.ANTIALIAS)
        bFrame4c = ImageTk.PhotoImage(image=bFrame4c)

        self.bFrame1 = bFrame1
        self.bFrame2 = bFrame2
        self.bFrame3 = bFrame3
        self.bFrame4 = bFrame4

        self.bFrame1c = bFrame1c
        self.bFrame2c = bFrame2c
        self.bFrame3c = bFrame3c
        self.bFrame4c = bFrame4c

        self.bgcanvas.bFrame1 = self.bFrame1
        self.bgcanvas.bFrame2 = self.bFrame2
        self.bgcanvas.bFrame3 = self.bFrame3
        self.bgcanvas.bFrame4 = self.bFrame4

        self.bgcanvas.bFrame1c = self.bFrame1c
        self.bgcanvas.bFrame2c = self.bFrame2c
        self.bgcanvas.bFrame3c = self.bFrame3c
        self.bgcanvas.bFrame4c = self.bFrame4c

        self.bFrame1action = self.bgcanvas.create_image(310, 528, image=self.bFrame1, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame1action, '<Button-1>', lambda x: self.click_frame(1, 0))
        self.bFrame2action = self.bgcanvas.create_image(643, 528, image=self.bFrame2, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame2action, '<Button-1>', lambda x: self.click_frame(2, 0))
        self.bFrame3action = self.bgcanvas.create_image(310, 960, image=self.bFrame3, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame3action, '<Button-1>', lambda x: self.click_frame(3, 1))
        self.bFrame4action = self.bgcanvas.create_image(643, 960, image=self.bFrame4, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame4action, '<Button-1>', lambda x: self.click_frame(4, 1))

    def click_frame(self, pos, fid):
        self.resetAllClick()
        frametype = FrameType()
        frametype.set_layout(fid)
        if pos == 1:
            self.bgcanvas.itemconfig(self.bFrame1action, image=self.bFrame1c)
            frametype.set_frame(2, 1)
        elif pos == 2:
            self.bgcanvas.itemconfig(self.bFrame2action, image=self.bFrame2c)
            frametype.set_frame(2, 2)
        elif pos == 3:
            self.bgcanvas.itemconfig(self.bFrame3action, image=self.bFrame3c)
            frametype.set_frame(2, 3)
        elif pos == 4:
            self.bgcanvas.itemconfig(self.bFrame4action, image=self.bFrame4c)
            frametype.set_frame(2, 4)

        # create NEXT button
        bNext = Image.open('./UI/Button/B4.png')
        bNext = bNext.resize((470, 180), Image.ANTIALIAS)
        bNext = ImageTk.PhotoImage(image=bNext)
        self.bgcanvas.bNext = bNext
        bNextaction = self.bgcanvas.create_image(540, 1670, image=bNext, anchor='nw')
        self.bgcanvas.tag_bind(bNextaction, '<Button-1>', lambda x: self.to_wait())

    def resetAllClick(self):
        self.bgcanvas.itemconfig(self.bFrame1action, image=self.bFrame1)
        self.bgcanvas.itemconfig(self.bFrame2action, image=self.bFrame2)
        self.bgcanvas.itemconfig(self.bFrame3action, image=self.bFrame3)
        self.bgcanvas.itemconfig(self.bFrame4action, image=self.bFrame4)

    def to_wait(self):
        self.after(400, self.destroy)
        subFrame = WaitingPage(self.original_frame)

    def to_main(self):
        self.after(400, self.destroy)
        self.original_frame.show()


class FrameSelect_3(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.attributes('-topmost', 'true')
        self.title("Frame")
        self.configure(bg='black')

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./UI/bg/7.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        bFrame1 = Image.open('./UI/frame/screen3/F1.png')
        bFrame1 = bFrame1.resize((288, 432), Image.ANTIALIAS)
        bFrame1 = ImageTk.PhotoImage(image=bFrame1)

        bFrame2 = Image.open('./UI/frame/screen3/F2.png')
        bFrame2 = bFrame2.resize((288, 432), Image.ANTIALIAS)
        bFrame2 = ImageTk.PhotoImage(image=bFrame2)

        bFrame3 = Image.open('./UI/frame/screen3/F3.png')
        bFrame3 = bFrame3.resize((288, 432), Image.ANTIALIAS)
        bFrame3 = ImageTk.PhotoImage(image=bFrame3)

        bFrame4 = Image.open('./UI/frame/screen3/F4.png')
        bFrame4 = bFrame4.resize((288, 432), Image.ANTIALIAS)
        bFrame4 = ImageTk.PhotoImage(image=bFrame4)

        bFrame5 = Image.open('./UI/frame/screen3/F5.png')
        bFrame5 = bFrame5.resize((288, 432), Image.ANTIALIAS)
        bFrame5 = ImageTk.PhotoImage(image=bFrame5)

        bFrame6 = Image.open('./UI/frame/screen3/F6.png')
        bFrame6 = bFrame6.resize((288, 432), Image.ANTIALIAS)
        bFrame6 = ImageTk.PhotoImage(image=bFrame6)

        bFrame7 = Image.open('./UI/frame/screen3/F7.png')
        bFrame7 = bFrame7.resize((288, 432), Image.ANTIALIAS)
        bFrame7 = ImageTk.PhotoImage(image=bFrame7)

        bFrame1c = Image.open('./UI/frame/screen3/F1-Click.png')
        bFrame1c = bFrame1c.resize((288, 432), Image.ANTIALIAS)
        bFrame1c = ImageTk.PhotoImage(image=bFrame1c)

        bFrame2c = Image.open('./UI/frame/screen3/F2-Click.png')
        bFrame2c = bFrame2c.resize((288, 432), Image.ANTIALIAS)
        bFrame2c = ImageTk.PhotoImage(image=bFrame2c)

        bFrame3c = Image.open('./UI/frame/screen3/F3-Click.png')
        bFrame3c = bFrame3c.resize((288, 432), Image.ANTIALIAS)
        bFrame3c = ImageTk.PhotoImage(image=bFrame3c)

        bFrame4c = Image.open('./UI/frame/screen3/F4-Click.png')
        bFrame4c = bFrame4c.resize((288, 432), Image.ANTIALIAS)
        bFrame4c = ImageTk.PhotoImage(image=bFrame4c)

        bFrame5c = Image.open('./UI/frame/screen3/F5-Click.png')
        bFrame5c = bFrame5c.resize((288, 432), Image.ANTIALIAS)
        bFrame5c = ImageTk.PhotoImage(image=bFrame5c)

        bFrame6c = Image.open('./UI/frame/screen3/F6-Click.png')
        bFrame6c = bFrame6c.resize((288, 432), Image.ANTIALIAS)
        bFrame6c = ImageTk.PhotoImage(image=bFrame6c)

        bFrame7c = Image.open('./UI/frame/screen3/F7-Click.png')
        bFrame7c = bFrame7c.resize((288, 432), Image.ANTIALIAS)
        bFrame7c = ImageTk.PhotoImage(image=bFrame7c)

        self.bFrame1 = bFrame1
        self.bFrame2 = bFrame2
        self.bFrame3 = bFrame3
        self.bFrame4 = bFrame4
        self.bFrame5 = bFrame5
        self.bFrame6 = bFrame6
        self.bFrame7 = bFrame7

        self.bFrame1c = bFrame1c
        self.bFrame2c = bFrame2c
        self.bFrame3c = bFrame3c
        self.bFrame4c = bFrame4c
        self.bFrame5c = bFrame5c
        self.bFrame6c = bFrame6c
        self.bFrame7c = bFrame7c

        self.bgcanvas.bFrame1 = self.bFrame1
        self.bgcanvas.bFrame2 = self.bFrame2
        self.bgcanvas.bFrame3 = self.bFrame3
        self.bgcanvas.bFrame4 = self.bFrame4
        self.bgcanvas.bFrame5 = self.bFrame5
        self.bgcanvas.bFrame6 = self.bFrame6
        self.bgcanvas.bFrame7 = self.bFrame7

        self.bgcanvas.bFrame1c = self.bFrame1c
        self.bgcanvas.bFrame2c = self.bFrame2c
        self.bgcanvas.bFrame3c = self.bFrame3c
        self.bgcanvas.bFrame4c = self.bFrame4c
        self.bgcanvas.bFrame5c = self.bFrame5c
        self.bgcanvas.bFrame6c = self.bFrame6c
        self.bgcanvas.bFrame7c = self.bFrame7c

        self.bFrame1action = self.bgcanvas.create_image(310, 312, image=self.bFrame1, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame1action, '<Button-1>', lambda x: self.click_frame(1, 5))
        self.bFrame2action = self.bgcanvas.create_image(643, 312, image=self.bFrame2, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame2action, '<Button-1>', lambda x: self.click_frame(2, 5))
        self.bFrame3action = self.bgcanvas.create_image(310, 744, image=self.bFrame3, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame3action, '<Button-1>', lambda x: self.click_frame(3, 6))
        self.bFrame4action = self.bgcanvas.create_image(643, 744, image=self.bFrame4, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame4action, '<Button-1>', lambda x: self.click_frame(4, 6))
        self.bFrame5action = self.bgcanvas.create_image(188, 1176, image=self.bFrame5, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame5action, '<Button-1>', lambda x: self.click_frame(5, 2))
        self.bFrame6action = self.bgcanvas.create_image(476, 1176, image=self.bFrame6, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame6action, '<Button-1>', lambda x: self.click_frame(6, 2))
        self.bFrame7action = self.bgcanvas.create_image(764, 1176, image=self.bFrame7, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame7action, '<Button-1>', lambda x: self.click_frame(7, 2))

    def click_frame(self, pos, fid):
        self.resetAllClick()
        frametype = FrameType()
        frametype.set_layout(fid)
        if pos == 1:
            self.bgcanvas.itemconfig(self.bFrame1action, image=self.bFrame1c)
            frametype.set_frame(3, 1)
        elif pos == 2:
            self.bgcanvas.itemconfig(self.bFrame2action, image=self.bFrame2c)
            frametype.set_frame(3, 2)
        elif pos == 3:
            self.bgcanvas.itemconfig(self.bFrame3action, image=self.bFrame3c)
            frametype.set_frame(3, 3)
        elif pos == 4:
            self.bgcanvas.itemconfig(self.bFrame4action, image=self.bFrame4c)
            frametype.set_frame(3, 4)
        elif pos == 5:
            self.bgcanvas.itemconfig(self.bFrame5action, image=self.bFrame5c)
            frametype.set_frame(3, 5)
        elif pos == 6:
            self.bgcanvas.itemconfig(self.bFrame6action, image=self.bFrame6c)
            frametype.set_frame(3, 6)
        elif pos == 7:
            self.bgcanvas.itemconfig(self.bFrame7action, image=self.bFrame7c)
            frametype.set_frame(3, 7)

        # create NEXT button
        bNext = Image.open('./UI/Button/B4.png')
        bNext = bNext.resize((470, 180), Image.ANTIALIAS)
        bNext = ImageTk.PhotoImage(image=bNext)
        self.bgcanvas.bNext = bNext
        bNextaction = self.bgcanvas.create_image(540, 1670, image=bNext, anchor='nw')
        self.bgcanvas.tag_bind(bNextaction, '<Button-1>', lambda x: self.to_wait())

    def resetAllClick(self):
        self.bgcanvas.itemconfig(self.bFrame1action, image=self.bFrame1)
        self.bgcanvas.itemconfig(self.bFrame2action, image=self.bFrame2)
        self.bgcanvas.itemconfig(self.bFrame3action, image=self.bFrame3)
        self.bgcanvas.itemconfig(self.bFrame4action, image=self.bFrame4)
        self.bgcanvas.itemconfig(self.bFrame5action, image=self.bFrame5)
        self.bgcanvas.itemconfig(self.bFrame6action, image=self.bFrame6)
        self.bgcanvas.itemconfig(self.bFrame7action, image=self.bFrame7)

    def to_wait(self):
        self.after(400, self.destroy)
        subFrame = WaitingPage(self.original_frame)

    def to_main(self):
        self.after(400, self.destroy)
        self.original_frame.show()


class FrameSelect_4(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.attributes('-topmost', 'true')
        self.title("Frame")
        self.configure(bg='black')

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./UI/bg/8.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        bFrame1 = Image.open('./UI/frame/screen4/F1.png')
        bFrame1 = bFrame1.resize((288, 432), Image.ANTIALIAS)
        bFrame1 = ImageTk.PhotoImage(image=bFrame1)

        bFrame2 = Image.open('./UI/frame/screen4/F2.png')
        bFrame2 = bFrame2.resize((288, 432), Image.ANTIALIAS)
        bFrame2 = ImageTk.PhotoImage(image=bFrame2)

        bFrame3 = Image.open('./UI/frame/screen4/F3.png')
        bFrame3 = bFrame3.resize((288, 432), Image.ANTIALIAS)
        bFrame3 = ImageTk.PhotoImage(image=bFrame3)

        bFrame4 = Image.open('./UI/frame/screen4/F4.png')
        bFrame4 = bFrame4.resize((288, 432), Image.ANTIALIAS)
        bFrame4 = ImageTk.PhotoImage(image=bFrame4)

        bFrame5 = Image.open('./UI/frame/screen4/F5.png')
        bFrame5 = bFrame5.resize((288, 432), Image.ANTIALIAS)
        bFrame5 = ImageTk.PhotoImage(image=bFrame5)

        bFrame6 = Image.open('./UI/frame/screen4/F6.png')
        bFrame6 = bFrame6.resize((288, 432), Image.ANTIALIAS)
        bFrame6 = ImageTk.PhotoImage(image=bFrame6)

        bFrame7 = Image.open('./UI/frame/screen4/F7.png')
        bFrame7 = bFrame7.resize((288, 432), Image.ANTIALIAS)
        bFrame7 = ImageTk.PhotoImage(image=bFrame7)

        bFrame1c = Image.open('./UI/frame/screen4/F1-Click.png')
        bFrame1c = bFrame1c.resize((288, 432), Image.ANTIALIAS)
        bFrame1c = ImageTk.PhotoImage(image=bFrame1c)

        bFrame2c = Image.open('./UI/frame/screen4/F2-Click.png')
        bFrame2c = bFrame2c.resize((288, 432), Image.ANTIALIAS)
        bFrame2c = ImageTk.PhotoImage(image=bFrame2c)

        bFrame3c = Image.open('./UI/frame/screen4/F3-Click.png')
        bFrame3c = bFrame3c.resize((288, 432), Image.ANTIALIAS)
        bFrame3c = ImageTk.PhotoImage(image=bFrame3c)

        bFrame4c = Image.open('./UI/frame/screen4/F4-Click.png')
        bFrame4c = bFrame4c.resize((288, 432), Image.ANTIALIAS)
        bFrame4c = ImageTk.PhotoImage(image=bFrame4c)

        bFrame5c = Image.open('./UI/frame/screen4/F5-Click.png')
        bFrame5c = bFrame5c.resize((288, 432), Image.ANTIALIAS)
        bFrame5c = ImageTk.PhotoImage(image=bFrame5c)

        bFrame6c = Image.open('./UI/frame/screen4/F6-Click.png')
        bFrame6c = bFrame6c.resize((288, 432), Image.ANTIALIAS)
        bFrame6c = ImageTk.PhotoImage(image=bFrame6c)

        bFrame7c = Image.open('./UI/frame/screen4/F7-Click.png')
        bFrame7c = bFrame7c.resize((288, 432), Image.ANTIALIAS)
        bFrame7c = ImageTk.PhotoImage(image=bFrame7c)

        self.bFrame1 = bFrame1
        self.bFrame2 = bFrame2
        self.bFrame3 = bFrame3
        self.bFrame4 = bFrame4
        self.bFrame5 = bFrame5
        self.bFrame6 = bFrame6
        self.bFrame7 = bFrame7

        self.bFrame1c = bFrame1c
        self.bFrame2c = bFrame2c
        self.bFrame3c = bFrame3c
        self.bFrame4c = bFrame4c
        self.bFrame5c = bFrame5c
        self.bFrame6c = bFrame6c
        self.bFrame7c = bFrame7c

        self.bgcanvas.bFrame1 = self.bFrame1
        self.bgcanvas.bFrame2 = self.bFrame2
        self.bgcanvas.bFrame3 = self.bFrame3
        self.bgcanvas.bFrame4 = self.bFrame4
        self.bgcanvas.bFrame5 = self.bFrame5
        self.bgcanvas.bFrame6 = self.bFrame6
        self.bgcanvas.bFrame7 = self.bFrame7

        self.bgcanvas.bFrame1c = self.bFrame1c
        self.bgcanvas.bFrame2c = self.bFrame2c
        self.bgcanvas.bFrame3c = self.bFrame3c
        self.bgcanvas.bFrame4c = self.bFrame4c
        self.bgcanvas.bFrame5c = self.bFrame5c
        self.bgcanvas.bFrame6c = self.bFrame6c
        self.bgcanvas.bFrame7c = self.bFrame7c

        self.bFrame1action = self.bgcanvas.create_image(310, 312, image=self.bFrame1, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame1action, '<Button-1>', lambda x: self.click_frame(1, 3))
        self.bFrame2action = self.bgcanvas.create_image(643, 312, image=self.bFrame2, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame2action, '<Button-1>', lambda x: self.click_frame(2, 3))
        self.bFrame3action = self.bgcanvas.create_image(310, 744, image=self.bFrame3, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame3action, '<Button-1>', lambda x: self.click_frame(3, 4))
        self.bFrame4action = self.bgcanvas.create_image(643, 744, image=self.bFrame4, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame4action, '<Button-1>', lambda x: self.click_frame(4, 4))
        self.bFrame5action = self.bgcanvas.create_image(188, 1176, image=self.bFrame5, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame5action, '<Button-1>', lambda x: self.click_frame(5, 2))
        self.bFrame6action = self.bgcanvas.create_image(476, 1176, image=self.bFrame6, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame6action, '<Button-1>', lambda x: self.click_frame(6, 2))
        self.bFrame7action = self.bgcanvas.create_image(764, 1176, image=self.bFrame7, anchor='nw')
        self.bgcanvas.tag_bind(self.bFrame7action, '<Button-1>', lambda x: self.click_frame(7, 2))

    def click_frame(self, pos, fid):
        self.resetAllClick()
        frametype = FrameType()
        frametype.set_layout(fid)
        if pos == 1:
            self.bgcanvas.itemconfig(self.bFrame1action, image=self.bFrame1c)
            frametype.set_frame(4, 1)
        elif pos == 2:
            self.bgcanvas.itemconfig(self.bFrame2action, image=self.bFrame2c)
            frametype.set_frame(4, 2)
        elif pos == 3:
            self.bgcanvas.itemconfig(self.bFrame3action, image=self.bFrame3c)
            frametype.set_frame(4, 3)
        elif pos == 4:
            self.bgcanvas.itemconfig(self.bFrame4action, image=self.bFrame4c)
            frametype.set_frame(4, 4)
        elif pos == 5:
            self.bgcanvas.itemconfig(self.bFrame5action, image=self.bFrame5c)
            frametype.set_frame(4, 5)
        elif pos == 6:
            self.bgcanvas.itemconfig(self.bFrame6action, image=self.bFrame6c)
            frametype.set_frame(4, 6)
        elif pos == 7:
            self.bgcanvas.itemconfig(self.bFrame7action, image=self.bFrame7c)
            frametype.set_frame(4, 7)

        # create NEXT button
        bNext = Image.open('./UI/Button/B4.png')
        bNext = bNext.resize((470, 180), Image.ANTIALIAS)
        bNext = ImageTk.PhotoImage(image=bNext)
        self.bgcanvas.bNext = bNext
        bNextaction = self.bgcanvas.create_image(540, 1670, image=bNext, anchor='nw')
        self.bgcanvas.tag_bind(bNextaction, '<Button-1>', lambda x: self.to_wait())

    def resetAllClick(self):
        self.bgcanvas.itemconfig(self.bFrame1action, image=self.bFrame1)
        self.bgcanvas.itemconfig(self.bFrame2action, image=self.bFrame2)
        self.bgcanvas.itemconfig(self.bFrame3action, image=self.bFrame3)
        self.bgcanvas.itemconfig(self.bFrame4action, image=self.bFrame4)
        self.bgcanvas.itemconfig(self.bFrame5action, image=self.bFrame5)
        self.bgcanvas.itemconfig(self.bFrame6action, image=self.bFrame6)
        self.bgcanvas.itemconfig(self.bFrame7action, image=self.bFrame7)

    def to_wait(self):
        self.after(600, self.destroy)
        subFrame = WaitingPage(self.original_frame)

    def to_main(self):
        self.after(400, self.destroy)
        self.original_frame.show()


class WaitingPage(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.attributes('-topmost', 'true')
        self.title("GET READY!")
        self.configure(bg='black')

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./UI/bg/0.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        frametype = FrameType()
        self.reso = frametype.get_layout()['reso']
        self.scale_size = 1

        self.vs = cv2.VideoCapture(0, cv2.CAP_DSHOW)  # capture video frames, 0 is your default video camera
        self.vs.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        self.vs.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
        self.panel = tk.Label(self)
        self.ax = int(self.reso[1] * self.scale_size)
        self.ay = int(self.reso[0] * self.scale_size)
        self.ix = int((screen_width / 2) - (self.reso[1] * self.scale_size / 2))
        self.panel.place(anchor='nw', x=self.ix, y=0, width=self.ax, height=self.ay)

        # btn = tk.Button(self, text="GO!", command=self.to_snap)
        # btn.pack()
        btn1 = Image.open('./UI/button/B5.png')
        btn1 = btn1.resize((150, 150), Image.ANTIALIAS)
        btn1 = ImageTk.PhotoImage(image=btn1)
        self.bgcanvas.btn1 = btn1
        btn1action = self.bgcanvas.create_image(465, 1550, image=btn1, anchor='nw')
        self.bgcanvas.tag_bind(btn1action, '<Button-1>', lambda x: self.to_snap())

        self.video_loop()

    def video_loop(self):
        ok, frame = self.vs.read()
        if ok:
            cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            cv2image = cv2.flip(cv2image, 1)
            self.current_image = Image.fromarray(cv2image)  # for save

            # crop and show
            # imgtoshow = imutils.resize(cv2image, width=700)
            imgtoshow = self.crop_img(cv2image, self.reso)  # for show
            imgtoshow = Image.fromarray(imgtoshow)
            # scale_size = 1  # for display only
            # scale_size = 1
            imgtoshow = imgtoshow.resize((int(self.reso[1] * self.scale_size), int(self.reso[0] * self.scale_size)))
            imgtk = ImageTk.PhotoImage(image=imgtoshow)
            self.panel.imgtk = imgtk
            self.panel.config(image=imgtk)

        self.after(40, self.video_loop)  # call the same function after 40 milliseconds

    def crop_img(self, img, reso):
        height = img.shape[0]
        width = img.shape[1]

        fhei = reso[0]
        fwid = reso[1]

        margin_y = (height - fhei)
        margin_x = (width - fwid)

        start_y = int(margin_y / 2)
        start_x = int(margin_x / 2)

        end_y = start_y + fhei
        end_x = start_x + fwid

        rimg = img[start_y:end_y, start_x:end_x, :]
        return rimg

    def to_main(self):
        self.after(400, self.destroy)
        self.original_frame.show()

    def to_snap(self):
        self.after(400, self.destroy)
        subFrame = Snapshot(self.original_frame)


class Snapshot(tk.Toplevel):
    def __init__(self, original):
        # print('init snapshot')
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.attributes('-topmost', 'true')
        self.title("booth")
        self.configure(bg='black')

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./UI/bg/0.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        """ Initialize application which uses OpenCV + Tkinter. It displays
            a video stream in a Tkinter window and stores current snapshot on disk """

        # print('before open capread')
        self.vs = cv2.VideoCapture(0, cv2.CAP_DSHOW)  # capture video frames, 0 is your default video camera
        self.vs.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        self.vs.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

        self.output_path = './image'  # store output path
        self.current_image = None  # current image from the camera

        frametype = FrameType()
        self.frameleft = frametype.get_layout()['shot']
        self.reso = frametype.get_layout()['reso']

        self.scale_size = 1

        self.panel = tk.Label(self)
        self.ax = int(self.reso[1] * self.scale_size)
        self.ay = int(self.reso[0] * self.scale_size)
        self.ix = int((screen_width / 2) - (self.reso[1] * self.scale_size / 2))
        self.panel.place(anchor='nw', x=self.ix, y=0, width=self.ax, height=self.ay)

        cntd6 = Image.open('./UI/countdown/6.png')
        cntd6 = cntd6.resize((150, 150), Image.ANTIALIAS)
        cntd6 = ImageTk.PhotoImage(image=cntd6)

        cntd5 = Image.open('./UI/countdown/5.png')
        cntd5 = cntd5.resize((150, 150), Image.ANTIALIAS)
        cntd5 = ImageTk.PhotoImage(image=cntd5)

        cntd4 = Image.open('./UI/countdown/4.png')
        cntd4 = cntd4.resize((150, 150), Image.ANTIALIAS)
        cntd4 = ImageTk.PhotoImage(image=cntd4)

        cntd3 = Image.open('./UI/countdown/3.png')
        cntd3 = cntd3.resize((150, 150), Image.ANTIALIAS)
        cntd3 = ImageTk.PhotoImage(image=cntd3)

        cntd2 = Image.open('./UI/countdown/2.png')
        cntd2 = cntd2.resize((150, 150), Image.ANTIALIAS)
        cntd2 = ImageTk.PhotoImage(image=cntd2)

        cntd1 = Image.open('./UI/countdown/1.png')
        cntd1 = cntd1.resize((150, 150), Image.ANTIALIAS)
        cntd1 = ImageTk.PhotoImage(image=cntd1)

        cntd0 = Image.open('./UI/countdown/0.png')
        cntd0 = cntd0.resize((150, 150), Image.ANTIALIAS)
        cntd0 = ImageTk.PhotoImage(image=cntd0)

        self.cntd = [cntd6, cntd6, cntd1, cntd2, cntd3, cntd4, cntd5, cntd5, cntd5, cntd5]
        self.bgcanvas.cntd0 = cntd0
        self.bgcanvas.cntd1 = cntd1
        self.bgcanvas.cntd2 = cntd2
        self.bgcanvas.cntd3 = cntd3
        self.bgcanvas.cntd4 = cntd4
        self.bgcanvas.cntd5 = cntd5
        self.bgcanvas.cntd6 = cntd6

        whitetop = Image.open('./UI/countdown/6.png')
        whitetop = whitetop.resize((self.reso[1], self.reso[0]), Image.ANTIALIAS)
        self.whitetop = ImageTk.PhotoImage(image=whitetop)

        self.whitecan = tk.Canvas(self, width=self.reso[1], height=self.reso[0], bd=0, highlightthickness=0)
        self.whitepane = self.whitecan.create_image(0, 0, image=self.whitetop, anchor='nw')

        # timer & label
        self.cntdcan = self.bgcanvas.create_image(465, int(50 + self.reso[0]), image=self.cntd[0], anchor='nw')

        # self.cntdcan.config(bg='')
        # self.label1 = tk.Label(self, text='', width=10, font=('Arial', 45), background='white')
        # self.label1.pack(padx=10)

        self.prv = None
        self.prv_scale = 0.5
        # self.pPanel = tk.Label(self)
        # self.pPanel.config(bg='black')
        # self.pPanel.place(anchor='se', x=1030, y=1870, width=int(self.reso[1]*self.prv_scale), height=int(self.reso[0]*self.prv_scale))
        # self.pPanel.cntd6 = cntd6
        # self.pPanel.config(image=cntd6)

        self.pPanel = self.bgcanvas.create_image(1030, 1870, anchor='se', image=cntd6)

        self.remaining = 0
        # self.delay = config['camera']['delay']
        self.delay = 5
        self.imnum = 1

        self.video_loop()
        self.loopSnap(self.delay + 1)

        # self.white = self.bgcanvas.create_image(465, 800, image=cntd0, anchor='nw')

        # self.bgcanvas.btn1 = btn1
        # btn1action = self.bgcanvas.create_image(400, 1550, image=btn1, anchor='nw')
        # self.bgcanvas.tag_bind(btn1action, '<Button-1>', lambda x: self.to_snap())

    def loopSnap(self, remaining=None):
        if remaining is not None:
            self.remaining = remaining

        if self.remaining <= 0:
            # self.label1.configure(text="snap")
            # print('frame left before snap : {}'.format(self.frameleft))

            self.snap()

            self.frameleft -= 1
            self.imnum += 1
            # print('frame left after snap : {}'.format(self.frameleft))
            if self.frameleft > 0:
                self.after(1000, self.loopSnap(self.delay + 2))
            else:
                self.to_loading()

        else:
            self.whitecan.place_forget()

            if self.remaining <= 1:
                self.whitecan.place(anchor='nw', x=self.ix, y=0)

            self.bgcanvas.itemconfig(self.cntdcan, image=self.cntd[self.remaining])
            self.remaining = self.remaining - 1
            self.after(1000, self.loopSnap)

    def snap(self):
        """ Take snapshot and save it to the file """
        ts = datetime.datetime.now()
        filename = "{} image{}.png".format(ts.strftime("%Y-%m-%d_%H-%M-%S"), self.imnum)  # construct filename
        p = os.path.join(self.output_path, filename)
        self.current_image.save(p, "png")

        prv = Image.open(self.output_path + '/' + filename)
        prv = prv.crop(self.prvPoints(prv.size))
        prv = prv.resize((int(self.reso[1] * self.prv_scale), int(self.reso[0] * self.prv_scale)), Image.ANTIALIAS)
        prv = ImageTk.PhotoImage(image=prv)
        self.prv = prv
        self.bgcanvas.itemconfig(self.pPanel, image=self.prv)

    def video_loop(self):
        """ Get frame from the video stream and show it in Tkinter """
        ok, frame = self.vs.read()
        if ok:
            cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            cv2image = cv2.flip(cv2image, 1)
            self.current_image = Image.fromarray(cv2image)  # for save

            # crop and show
            # imgtoshow = imutils.resize(cv2image, width=700)
            imgtoshow = self.crop_img(cv2image, self.reso)  # for show
            imgtoshow = Image.fromarray(imgtoshow)
            # scale_size = 1  # for display only
            # scale_size = 1
            imgtoshow = imgtoshow.resize((int(self.reso[1] * self.scale_size), int(self.reso[0] * self.scale_size)))
            imgtk = ImageTk.PhotoImage(image=imgtoshow)
            self.panel.imgtk = imgtk
            self.panel.config(image=imgtk)

        self.after(40, self.video_loop)  # call the same function after 40 milliseconds

    def destructor(self):
        """ Destroy the root object and release all resources """
        # print("[INFO] closing...")
        self.vs.release()  # release web camera
        cv2.destroyAllWindows()  # it is not mandatory in this application

    def crop_img(self, img, reso):
        height = img.shape[0]
        width = img.shape[1]

        fhei = reso[0]
        fwid = reso[1]

        margin_y = (height - fhei)
        margin_x = (width - fwid)

        start_y = int(margin_y / 2)
        start_x = int(margin_x / 2)

        end_y = start_y + fhei
        end_x = start_x + fwid

        rimg = img[start_y:end_y, start_x:end_x, :]
        return rimg

    def prvPoints(self, size):
        w = size[0]
        h = size[1]
        fw = self.reso[1]
        fh = self.reso[0]
        marginx = int(w - fw)
        marginy = int(h - fh)
        left = int(marginx / 2)
        right = int(w - (marginx / 2))
        top = int(marginy / 2)
        bot = int(h - (marginy / 2))
        # print((left, top, right, bot))
        return left, top, right, bot

    def to_loading(self):
        self.destructor()
        self.destroy()
        subFrame = Loading(self.original_frame)


class Loading(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.attributes('-topmost', 'true')
        self.title("loading")
        self.configure(bg='black')

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./UI/bg/10 Loading.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        # btn = tk.Button(self, text="Main Menu", command=self.to_main)
        # btn.pack()

        self.imgpath = './image/'
        self.blankpath = './UI/blank/'

        frametype = FrameType()
        self.layout = frametype.get_layout()
        self.framepath = frametype.get_frame()['path']
        self.shotamount = self.layout['shot']
        # print('layout : {}'.format(self.layout))

        # {'shot': 3, 'col': 2, 'row': 3, 'reso': [468, 520]}

        # time.sleep(5)
        # self.replaceimg()
        # self.after(5000, self.replaceimg())
        # self.upload_img()
        # self.replaceimg()
        self.imbuf = 1
        self.buffer()

    def buffer(self):
        if self.imbuf < 5:
            self.imbuf += 1
            # print('{}'.format(self.imbuf))
            self.after(1000, self.buffer)
        else:
            self.replaceimg()

    def prepareimg(self, img, reso):
        # crop
        height = img.shape[0]
        width = img.shape[1]

        fhei = reso[0]
        fwid = reso[1]

        margin_y = (height - fhei)
        margin_x = (width - fwid)

        start_y = int(margin_y / 2)
        start_x = int(margin_x / 2)

        end_y = start_y + fhei
        end_x = start_x + fwid

        rimg = img[start_y:end_y, start_x:end_x, :]
        return rimg

    def replaceimg(self):
        bimg = cv2.imread(self.blankpath + 'white.png')

        listimg = os.listdir(self.imgpath)
        listimg.sort()
        # if self.shotamount == 3:
        #     listimg = listimg[-3:]
        # else:
        #     listimg = listimg[-4:]
        listimg = listimg[-self.shotamount:]

        # copy of blank white image
        fnimg = bimg.copy()
        # fnimg = imutils.resize(fnimg, height=1560)  # H1560 W1040 for 720p camera
        # fnimg = imutils.resize(fnimg, height=2400)  # H2400 W1600 for 1080p camera
        fnimg = imutils.resize(fnimg, height=self.layout.get('fwhite'))

        for i in range(len(listimg)):
            listimg[i] = './image/' + listimg[i]

        imgobj = dict()
        indexnum = 1  # naming

        reso = self.layout['reso']

        for i in range(self.shotamount):
            kv = {indexnum: self.prepareimg(cv2.imread(listimg[i]), reso)}
            imgobj.update(kv)
            indexnum += 1
        # print(imgobj)
        # blank reso = H1560 W1040
        # margin 156 top = 78, bot = 78
        # if self.layout['mt'] == 5
        margin = int(fnimg.shape[0] * (self.layout['mt'] / 100))

        start_y = margin
        start_x = 0

        px_y = self.layout['reso'][0]
        px_x = self.layout['reso'][1]

        if self.layout['mode'] == 'n':
            sh = 1
            for col in range(self.layout['col']):
                for row in range(self.layout['row']):
                    fnimg[start_y:start_y + px_y, start_x:start_x + px_x, :] = imgobj[row + sh][0:reso[0], 0:reso[1], :]
                    start_y = start_y + px_y
                start_y = margin
                start_x = start_x + px_x
                sh += (self.layout['shot'] / 2)
                # for row in range(self.layout['row']):
                #     fnimg[start_y:start_y + px_y, start_x:start_x + px_x, :] = imgobj[col + sh][0:reso[0], 0:reso[1], :]
                #     start_y = start_y + px_y
        elif self.layout['mode'] == 'd':
            for col in range(self.layout['col']):
                for row in range(self.layout['row']):
                    fnimg[start_y:start_y + px_y, start_x:start_x + px_x, :] = imgobj[row + 1][0:reso[0], 0:reso[1], :]
                    start_y = start_y + px_y
                start_y = margin
                start_x = start_x + px_x

        cv2.imwrite('output/buffer.png', fnimg)

        output = self.placeFrame()
        cv2.imwrite('./output/output.png', output)
        # print('done')
        self.upload_img()

    def placeFrame(self):
        # print('place frame')
        frame_to_place = cv2.imread(self.framepath, -1)
        org_img = cv2.imread("./output/buffer.png")

        frame_to_place = self.frameResize(frame_to_place)

        back_image = org_img.copy()
        front_image = frame_to_place.copy()

        start_y, end_y = 0, back_image.shape[0]
        start_x, end_x = 0, back_image.shape[1]

        alpha_f = front_image[:, :, 3] / 255.0
        alpha_b = 1.0 - alpha_f

        for c in range(0, 3):
            back_image[start_y:end_y, start_x:end_x, c] = (alpha_f * front_image[:, :, c] +
                                                           alpha_b * back_image[start_y:end_y, start_x:end_x, c])

        # added_image = cv2.addWeighted(back_image, 1, front_image, 1, 0)
        # print('finished')
        # return added_image
        return back_image

    def frameResize(self, org_frame):
        # print('resizing frame')
        # org_frame = imutils.resize(org_frame, height=1560)  # for 1280
        # org_frame = imutils.resize(org_frame, height=2400)
        if self.layout.get('fwhite') == 2400:
            org_frame = imutils.resize(org_frame, height=self.layout.get('fwhite'))
        else:
            org_frame = imutils.resize(org_frame, height=3240)
        return org_frame

    def upload_img(self):
        # url = 'http://192.168.1.8:3049/api/upload/foto'
        url = 'https://fotomachine.co/API/upload/foto'
        image = {'image': open('./output/output.png', 'rb')}
        res = requests.post(url, files=image)
        data = res.json()
        # print(data)
        self.genQR(data['path'])

    def genQR(self, path):
        url = 'https://fotomachine.co/API/image/'
        path = path
        fpath = url + path
        # print(fpath)
        img = qrcode.make(fpath)
        type(img)
        img.save('./output/qr_download.png')
        self.to_showcase()

    def to_showcase(self):
        self.after(400, self.destroy)
        subFrame = Showcase(self.original_frame)

    def to_main(self):
        self.after(400, self.destroy)
        self.original_frame.show()


class Showcase(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.attributes('-topmost', 'true')
        self.title("Result")
        self.configure(bg='black')

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./UI/bg/9.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        frametype = FrameType()
        self.layout = frametype.get_layout()

        btn = tk.Button(self, text="Main Menu", command=self.to_main)
        btn.pack()

        self.panel = tk.Label(self.bgcanvas)
        self.panel.place(x=70, y=1080)

        self.qrpan = tk.Label(self.bgcanvas)
        self.qrpan.place(x=610, y=1400)
        self.countdown = 121
        self.ctxt = tk.Label(self, text='120', width=3, font=('Arial', 50), fg='#fff', background='#000')
        self.ctxt.place(x=950, y=20)

        self.config_img()

    def config_img(self):
        img = Image.open('./output/output.png')
        img = img.resize((480, 720), Image.ANTIALIAS)
        imgtk = ImageTk.PhotoImage(image=img)
        self.panel.imgtk = imgtk
        self.panel.config(image=imgtk)
        # # print(self.panel.imgtk)

        qrimg = Image.open('./output/qr_download.png')
        qrimg = qrimg.resize((400, 400))
        qrimgtk = ImageTk.PhotoImage(image=qrimg)
        self.qrpan.qrimgtk = qrimgtk
        self.qrpan.config(image=qrimgtk)
        # # print(self.qrpan.qrimgtk)

        # self.to_printer()
        self.to_printer2()

    def to_printer(self):
        PRINTER_DEFAULTS = {"DesiredAccess": win32print.PRINTER_ALL_ACCESS}
        pHandle = win32print.OpenPrinter('DS-RX1', PRINTER_DEFAULTS)
        properties = win32print.GetPrinter(pHandle, 2)

        cuthalf = self.layout['cut']
        # glossy
        # cut_2in = b'DINU"\x008\x01\x94\x03|\x00\x04Oy\xdf\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x04\x00\x01\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x008\x01\x00\x00SMTJ\x00\x00\x00\x00\x10\x00(\x01D\x00S\x00-\x00R\x00X\x001\x00\x00\x00InputBin\x00FORMSOURCE\x00RESDLL\x00UniresDLL\x00Orientation\x00LANDSCAPE_CC270\x00Resolution\x00Option1\x00PaperSize\x00PC\x00PrintMargin\x00MarginOff\x00OVERCOATTYPE\x00OPTYPE_LUSTER\x00PRINTBUFFCONTROL\x00PBC_CLEAR\x00CUTTERCONTROL\x00CUT_2INCH\x00MediaType\x00STANDARD\x00ColorMode\x0024bpp\x00Halftone\x00HT_PATSIZE_SUPERCELL_M\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00|\x00\x00\x00TFSM\x04\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00\x07\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00'
        # cut_std = b'DINU"\x008\x01\x94\x03|\x00\x04Oy\xdf\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x04\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x008\x01\x00\x00SMTJ\x00\x00\x00\x00\x10\x00(\x01D\x00S\x00-\x00R\x00X\x001\x00\x00\x00InputBin\x00FORMSOURCE\x00RESDLL\x00UniresDLL\x00Orientation\x00LANDSCAPE_CC270\x00Resolution\x00Option1\x00PaperSize\x00PC\x00PrintMargin\x00MarginOff\x00OVERCOATTYPE\x00OPTYPE_LUSTER\x00PRINTBUFFCONTROL\x00PBC_CLEAR\x00CUTTERCONTROL\x00CUT_STANDARD\x00MediaType\x00STANDARD\x00ColorMode\x0024bpp\x00Halftone\x00HT_PATSIZE_SUPERCELL_M\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00|\x00\x00\x00TFSM\x04\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        # matte
        cut_2in = b'DINU"\x008\x01\x94\x03|\x00\x04Oy\xdf\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x04\x00\x01\x00\x01\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x008\x01\x00\x00SMTJ\x00\x00\x00\x00\x10\x00(\x01D\x00S\x00-\x00R\x00X\x001\x00\x00\x00InputBin\x00FORMSOURCE\x00RESDLL\x00UniresDLL\x00Orientation\x00LANDSCAPE_CC270\x00Resolution\x00Option1\x00PaperSize\x00PC\x00PrintMargin\x00MarginOff\x00OVERCOATTYPE\x00OPTYPE_MATTE1\x00PRINTBUFFCONTROL\x00PBC_CLEAR\x00CUTTERCONTROL\x00CUT_2INCH\x00MediaType\x00STANDARD\x00ColorMode\x0024bpp\x00Halftone\x00HT_PATSIZE_SUPERCELL_M\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00|\x00\x00\x00TFSM\x04\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\x00\x00\x01\x00\x00\x00\x07\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00'
        cut_std = b'DINU"\x008\x01\x94\x03|\x00\x04Oy\xdf\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x04\x00\x01\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x008\x01\x00\x00SMTJ\x00\x00\x00\x00\x10\x00(\x01D\x00S\x00-\x00R\x00X\x001\x00\x00\x00InputBin\x00FORMSOURCE\x00RESDLL\x00UniresDLL\x00Orientation\x00LANDSCAPE_CC270\x00Resolution\x00Option1\x00PaperSize\x00PC\x00PrintMargin\x00MarginOff\x00OVERCOATTYPE\x00OPTYPE_MATTE1\x00PRINTBUFFCONTROL\x00PBC_CLEAR\x00CUTTERCONTROL\x00CUT_STANDARD\x00MediaType\x00STANDARD\x00ColorMode\x0024bpp\x00Halftone\x00HT_PATSIZE_SUPERCELL_M\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00|\x00\x00\x00TFSM\x04\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\x00\x00\x01\x00\x00\x00\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        p = properties
        if cuthalf:
            p['pDevMode'].DriverData = cut_2in
            win32print.SetPrinter(pHandle, 2, p, 0)
        else:
            p['pDevMode'].DriverData = cut_std
            win32print.SetPrinter(pHandle, 2, p, 0)

        bmp = Image.open('./output/output.png')

        hDC = win32ui.CreateDC()
        hDC.CreatePrinterDC(win32print.GetDefaultPrinter())
        hDC.StartDoc("FOTO")
        hDC.StartPage()

        dib = ImageWin.Dib(bmp)
        x1 = 0
        y1 = 0
        x2 = 1239  # full paper 4.13" * 6.15" * 300dpi
        y2 = 1845

        dib.draw(hDC.GetHandleOutput(), (x1, y1, x2, y2))

        hDC.EndPage()
        hDC.EndDoc()
        hDC.DeleteDC()

        self.buffer()

    def to_printer2(self):
        self.buffer()

    def buffer(self):
        if self.countdown > 0:
            self.countdown -= 1
            self.ctxt.config(text=str(self.countdown))
            self.after(1000, self.buffer)
        else:
            self.to_main()

    def to_main(self):
        self.destroy()
        self.original_frame.show()


class ChoosePayment(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.attributes('-topmost', 'true')
        self.title("Choose Payment")
        self.configure(bg='black')

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./UI/bg/2.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        btn1 = Image.open('./UI/button/B1.png')
        btn1 = btn1.resize((860, 400), Image.ANTIALIAS)
        btn1 = ImageTk.PhotoImage(image=btn1)
        self.bgcanvas.btn1 = btn1
        btn1action = self.bgcanvas.create_image(110, 520, image=btn1, anchor='nw')
        self.bgcanvas.tag_bind(btn1action, '<Button-1>', lambda x: self.open_payment())

        btn2 = Image.open('./UI/button/B2.png')
        btn2 = btn2.resize((860, 400), Image.ANTIALIAS)
        btn2 = ImageTk.PhotoImage(image=btn2)
        self.bgcanvas.btn2 = btn2
        btn2action = self.bgcanvas.create_image(110, 970, image=btn2, anchor='nw')
        self.bgcanvas.tag_bind(btn2action, '<Button-1>', lambda y: self.open_coupon())

        # button3 = tk.Button(self, text='bypass', width=40, height=40, command=self.bypass)
        # button3.place(width=100, height=100)

        # button4 = tk.Button(self, text='bypass2', width=40, height=40, command=self.bypass2)
        # button4.place(x=100, y=100, width=100, height=100)

    def open_payment(self):
        self.destroy()
        subFrame = Payment(self.original_frame)

    def open_coupon(self):
        self.after(400, self.destroy)
        subFrame = Coupon(self.original_frame)

    # def bypass2(self):
    #     self.after(400, self.destroy)
    #     subFrame = Showcase(self.original_frame)

    def bypass(self):
        # subFrame = Loading(self)
        self.after(400, self.destroy)

        if config.get('screen').get('frame_screen') == 1:
            subFrame = FrameSelect_1(self.original_frame)
        elif config.get('screen').get('frame_screen') == 2:
            subFrame = FrameSelect_2(self.original_frame)
        elif config.get('screen').get('frame_screen') == 3:
            subFrame = FrameSelect_3(self.original_frame)
        elif config.get('screen').get('frame_screen') == 4:
            subFrame = FrameSelect_4(self.original_frame)
        else:
            subFrame = FrameSelect_1(self.original_frame)

    def to_main(self):
        self.after(400, self.destroy)
        self.original_frame.show()


class BLayer(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.title("")
        self.attributes('-topmost', 'false')
        self.configure(bg='black')
        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()
        BGimg = Image.open('./UI/bg/0.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')


########################################################################
class MyApp:
    def __init__(self, parent):
        self.root = parent
        self.root.title("FOTO")
        self.root.wm_attributes("-topmost", True)

        self.canvas = tk.Canvas(self.root, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.canvas.pack()
        self.root.configure(bg='black')

        self.BG = Image.open('./UI/bg/1.jpg')
        self.BG = self.BG.resize((screen_width, screen_height), Image.ANTIALIAS)
        self.BG = ImageTk.PhotoImage(image=self.BG)
        self.canvas.BGimg = self.BG
        self.BGaction = self.canvas.create_image(0, 0, image=self.BG, anchor='nw')
        self.canvas.tag_bind(self.BGaction, '<Button-1>', lambda x: self.choose_payment())
        subFrame = BLayer(self)

    def hide(self):
        self.root.withdraw()

    def choose_payment(self):
        self.root.after(400, self.hide)
        subFrame = ChoosePayment(self)

    def bypass_to_frame(self):
        self.root.after(400, self.hide)
        if config.get('screen').get('frame_screen') == 1:
            subFrame = FrameSelect_1(self)
        elif config.get('screen').get('frame_screen') == 2:
            subFrame = FrameSelect_2(self)
        elif config.get('screen').get('frame_screen') == 3:
            subFrame = FrameSelect_3(self)
        elif config.get('screen').get('frame_screen') == 4:
            subFrame = FrameSelect_4(self)
        else:
            subFrame = FrameSelect_1(self)

    def show(self):
        # self.BGaction = self.canvas.create_image(0, 0, image=self.BG, anchor='nw')
        # self.canvas.tag_bind(self.BGaction, '<Button-1>', lambda x: self.choose_payment())
        self.root.update()
        self.root.deiconify()


if __name__ == "__main__":
    root = tk.Tk()
    config2 = dict()
    # with open("config.json") as config_file:
    #     # config2 = json.load(config_file)
    #     config2.update(json.load(config_file))
    # config_file.close()
    # print('main config : {}'.format(config2))

    sys.excepthook = cef.ExceptHook
    settings = {}
    cef.Initialize(settings=settings)

    screen_width, screen_height = [int(i) for i in (config['screen']['screen_resolution'].split('x'))]
    root.geometry(config.get('screen').get('screen_resolution'))
    root.configure(bg='black')
    root.attributes('-fullscreen', config.get('screen').get('full_screen'))
    root.wm_attributes("-topmost", True)
    # root.wm_attributes("-transparentcolor", "white")

    app = MyApp(root)


    def on_closing():
        if msgbox.askokcancel("Quit", "Do you want to quit?"):
            root.destroy()


    root.protocol("WM_DELETE_WINDOW", on_closing)
    root.mainloop()
    cef.Shutdown()
