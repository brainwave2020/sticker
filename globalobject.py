class Snapshot(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("booth")
        """ Initialize application which uses OpenCV + Tkinter. It displays
            a video stream in a Tkinter window and stores current snapshot on disk """
        self.vs = cv2.VideoCapture(0)  # capture video frames, 0 is your default video camera
        time.sleep(1)
        self.output_path = './image'  # store output path
        self.current_image = None  # current image from the camera

        # self.destructor function gets fired when the window is closed

        self.panel = tk.Label(self)  # initialize image panel
        self.panel.pack(padx=10, pady=10)

        # timer & label
        self.label1 = tk.Label(self, text='', width=10, font=('Arial', 20))
        self.label1.pack(padx=10)
        self.remaining = 0
        self.delay = config['camera']['delay']
        self.imnum = 1

        frametype = FrameType()
        self.frameleft = frametype.get_img_num()
        # print(frametype.get_img_num())
        self.loopSnap(self.delay)

        # create a button, that when pressed, will take the current frame and save it to file
        btn = tk.Button(self, text="Snapshot!", command=self.snap)
        # btn.pack(fill="x", expand=True, padx=10, pady=10)

        # start a self.video_loop that constantly pools the video sensor
        # for the most recently read frame
        self.video_loop()

    def loopSnap(self, remaining=None):
        if remaining is not None:
            self.remaining = remaining

        if self.remaining <= 0:
            self.label1.configure(text="snap")
            print('frame left before snap : {}'.format(self.frameleft))

            # if self.frameleft > 0:
            self.snap()
            # time.sleep(2)
            self.frameleft -= 1
            self.imnum += 1
            print('frame left after snap : {}'.format(self.frameleft))
            if self.frameleft > 0:
                self.loopSnap(self.delay)
            else:
                print('finished in')
                self.to_loading()
            # else:
            #     print('finished')
            #     # to process image
                self.to_loading()
        else:
            self.label1.configure(text="%d" % self.remaining)
            self.remaining = self.remaining - 1
            self.after(1000, self.loopSnap)

    def snap(self):
        """ Take snapshot and save it to the file """
        ts = datetime.datetime.now()  # grab the current timestamp
        filename = "{} image{}.png".format(ts.strftime("%Y-%m-%d_%H-%M-%S"), self.imnum)  # construct filename
        p = os.path.join(self.output_path, filename)  # construct output path
        self.current_image.save(p, "png")  # save image as jpeg file
        print("[INFO] saved {}".format(filename))

    def video_loop(self):
        """ Get frame from the video stream and show it in Tkinter """
        ok, frame = self.vs.read()  # read frame from video stream
        if ok:  # frame captured without any errors
            cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)  # convert colors from BGR to RGBA
            cv2image = imutils.resize(cv2image, width=1080)  # resizing
            self.current_image = Image.fromarray(cv2image)  # convert image for PIL
            imgtk = ImageTk.PhotoImage(image=self.current_image)  # convert image for tkinter
            self.panel.imgtk = imgtk  # anchor imgtk so it does not be deleted by garbage-collector
            self.panel.config(image=imgtk)  # show the image
        self.after(40, self.video_loop)  # call the same function after 40 milliseconds

    def destructor(self):
        """ Destroy the root object and release all resources """
        print("[INFO] closing...")
        self.vs.release()  # release web camera
        cv2.destroyAllWindows()  # it is not mandatory in this application

    def to_loading(self):
        self.destructor()
        self.destroy()
        subFrame = Loading(self.original_frame)