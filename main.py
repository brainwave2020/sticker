import tkinter as tk
from tkinter import messagebox as msgbox
import time
import webview
import requests
import hashlib
import datetime
import os
import json

import cv2
import numpy as np
from PIL import Image, ImageTk
import imutils


########################################################################
class Payment(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("QR Payment")

        btn01 = tk.Button(self, text="Main Menu", command=self.to_main)
        btn02 = tk.Button(self, text="Frame", command=self.open_webview)
        btn01.pack()
        btn02.pack(pady=40)

        self.QRwindow = webview.create_window(url='', title='QR payment', width=800, height=700,
                                              on_top=True, resizable=False, frameless=True)
        self.url_makepayment = 'https://sandbox-appsrv2.chillpay.co/api/v2/Payment/'
        self.url_checkstatus = 'https://sandbox-appsrv2.chillpay.co/api/v2/PaymentStatus/'
        self.cpSKEY = '2nN0JQZpktNC44MjYQzeo4ZKzWCVjCv5FypHRuI2T9jD8nU4EwtVPYmRXZ2XF7YT1Jhpq7nbGQ3kc8buVjITDpYbnE9EKqaB4tVrns1RgSVSnBThsRXGGvzdGcdLSY7KX3dxIC2cSdMY8lnzrItYbNylVTLL9CqAXxvNd'
        self.APIKey = 'yQ8TPLJriPUqKRBUZfQqVyn42wtDBgw3dSrvvLxXWUytFTEGXGNc6bRXCawRm7tZ'
        self.payload_headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        self.payload_payment = {
            'MerchantCode': 'M032829',
            'OrderNo': '00001',
            'CustomerId': '001',
            'Amount': '20000',
            'ChannelCode': 'bank_qrcode',
            'Currency': '764',
            'RouteNo': '1',
            'IPAddress': '127.0.0.1',
            'APIKey': 'HFDFNVSgdMuLYdL2Kt8ZE6DfZTcLHPLwY5wD9QLp2xRjxnAOuglP6VVa5gQcnLWo',
            'Checksum': '',
        }
        self.payload_result = dict()
        self.payload_paymentstatus = {
            'MerchantCode': 'M032829',
            'TransactionId': '',
            'APIKey': 'HFDFNVSgdMuLYdL2Kt8ZE6DfZTcLHPLwY5wD9QLp2xRjxnAOuglP6VVa5gQcnLWo',
            'Checksum': '',
        }
        self.payment_makepayment()

    def payment_makepayment(self):
        # prepare payload
        s2h = self.prepares2h(self.payload_payment)
        hhStr = self.hashStr(s2h)
        self.payload_payment.update(Checksum=hhStr)

        # requests payment
        res = requests.post(url=self.url_makepayment, data=self.payload_payment, headers=self.payload_headers)
        data = res.json()

        if data.get('Status') == 0:
            self.payload_result.update(data)
            self.payload_paymentstatus.update(TransactionId=data.get('TransactionId'))
            # prepare payload status
            s2h = self.prepares2h(self.payload_paymentstatus)
            hhStr = self.hashStr(s2h)
            self.payload_paymentstatus.update(Checksum=hhStr)
            # start webview
            webview.start(self.webview_handle, self.QRwindow)
        else:
            msgbox.showerror(title='FAILED', message='make requests fail')
            self.to_main()
        # self.QRwindow.load_url(self.payload_result.get('PaymentUrl'))
        # print(self.payload_payment)

    def webview_handle(self, QRwindow):
        print('url = ', self.payload_result.get('PaymentUrl'))
        QRwindow.load_url(self.payload_result.get('PaymentUrl'))

        keepcall = True
        while keepcall:
            time.sleep(1)
            resStat = self.payment_status_keepcalling()
            print('loop got status ', resStat)
            if resStat == 0:
                keepcall = False
                print('FINISHED')
                self.QRwindow.destroy()
                self.to_frame()
            elif resStat in [1, 2, 3]:
                keepcall = False
                self.QRwindow.destroy()
                msgbox.showerror(title='ERROR', message='Payment Failed.')
                self.to_main()

    def payment_status_keepcalling(self):
        res = requests.post(url=self.url_checkstatus, data=self.payload_paymentstatus, headers=self.payload_headers)
        resRaw = res.json()
        resStat = resRaw.get('PaymentStatus')
        print('keepcall return status', resStat)
        return resStat

    def prepares2h(self, data):
        s2h = ''
        for i in data:
            s2h += str(data.get(i))
        s2h += self.cpSKEY
        return s2h

    def hashStr(self, s2h):
        hhStr = hashlib.md5(str(s2h).encode('utf-8')).hexdigest()
        return hhStr

    def to_main(self):
        self.destroy()
        self.original_frame.show()

    def to_frame(self):
        self.destroy()
        subFrame = FrameSelect_1(self.original_frame)

    def open_webview(self):
        webview.start()

    # def close_webview(self, QRwindow):


########### demo
# class Demo(tk.Toplevel):
#     def __init__(self, original):
#         self.original_frame = original
#         tk.Toplevel.__init__(self)
#         self.geometry("400x711")
#         self.title("Coupon")
#
#         btn = tk.Button(self, text="Main Menu", command=self.to_main)
#         btn.pack()
#
#     def to_main(self):
#         self.destroy()
#         self.original_frame.show()
############

class Coupon(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.attributes('-fullscreen', config.get('screen').get('full_screen'))
        self.title("Voucher")
        self.configure(bg='black')

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./UI/bg/4.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        btn1 = Image.open('./UI/button/B3.png')
        btn1 = btn1.resize((470, 180), Image.ANTIALIAS)
        btn1 = ImageTk.PhotoImage(image=btn1)
        self.bgcanvas.btn1 = btn1
        btn1action = self.bgcanvas.create_image(70, 1670, image=btn1, anchor='nw')
        self.bgcanvas.tag_bind(btn1action, '<Button-1>', lambda x: self.to_main())

        # self.panel2 = tk.Label(self)
        # self.panel.place(anchor='nw', x=10, y=10, width=200, height=200)

        # self.vs = cv2.VideoCapture(1)
        self.vs = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        self.qrCodeDetector = cv2.QRCodeDetector()
        self.current_image = None
        self.img_scan = None

        self.panel = tk.Label(self)
        # self.panel.pack(padx=10, pady=10)
        self.panel.place(anchor='nw', x=270, y=480, width=540, height=540)

        # btn = tk.Button(self, text="Main Menu", command=self.to_main)
        # btn.pack()
        self.couponcode = ''

        self.video_loop()
        self.qrreader()

    def video_loop(self):
        ok, frame = self.vs.read()
        if ok:
            cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            cv2image = imutils.resize(cv2image, width=600)
            self.current_image = Image.fromarray(cv2image)
            self.img_scan = frame
            imgtk = ImageTk.PhotoImage(image=self.current_image)
            self.panel.imgtk = imgtk
            self.panel.config(image=imgtk)
        self.after(40, self.video_loop)

    def destructor(self):
        # print("[INFO] qr reader closing...")
        self.vs.release()
        cv2.destroyAllWindows()

    def qrreader(self):
        data, points, _ = self.qrCodeDetector.detectAndDecode(self.img_scan)
        print('scanning')
        if data:
            print('QR founded')
            self.couponcode = data
            self.check_coupon()
        else:
            self.after(1000, self.qrreader)

    def check_coupon(self):
        self.vs.release()
        # request
        payload = json.dumps({'code': self.couponcode})
        # print(payload)
        # res = requests.post(url='', data=payload)
        if self.couponcode == 'this is test code':
            self.to_frame()
        else:
            msgbox.showerror(title='FAILED', message='QR code invalid!')
            self.to_main()
        # if success : to_frame

    def to_main(self):
        self.destroy()
        self.original_frame.show()

    def to_frame(self):
        self.destructor()
        self.destroy()
        # subFrame = FrameSelect_1(self.original_frame)
        if config.get('screen').get('frame_screen') == 1:
            subFrame = FrameSelect_1(self.original_frame)
        elif config.get('screen').get('frame_screen') == 2:
            subFrame = FrameSelect_2(self.original_frame)
        elif config.get('screen').get('frame_screen') == 3:
            subFrame = FrameSelect_3(self.original_frame)
        elif config.get('screen').get('frame_screen') == 4:
            subFrame = FrameSelect_4(self.original_frame)
        else:
            subFrame = FrameSelect_1(self.original_frame)


# singleton for frametype
def singleton(cls, *args, **kw):
    instances = {}

    def _singleton(*args, **kw):
        if cls not in instances:
            instances[cls] = cls(*args, **kw)
        return instances[cls]

    return _singleton


@singleton
class FrameType:
    frametype = 0
    img_num = 0

    layout = dict()

    layoutlist = {
        0: {'shot': 4, 'col': 2, 'row': 2, 'reso': [520, 520], 'mt': 0, 'cut': True},
        1: {'shot': 4, 'col': 2, 'row': 2, 'reso': [520, 520], 'mt': 0, 'cut': False},
        2: {'shot': 4, 'col': 2, 'row': 2, 'reso': [702, 520], 'mt': 5, 'cut': False},
        3: {'shot': 3, 'col': 2, 'row': 3, 'reso': [468, 520], 'mt': 5, 'cut': True},
        4: {'shot': 6, 'col': 2, 'row': 3, 'reso': [468, 520], 'mt': 5, 'cut': True},
        5: {'shot': 4, 'col': 2, 'row': 4, 'reso': [351, 520], 'mt': 5, 'cut': True},
        6: {'shot': 8, 'col': 2, 'row': 4, 'reso': [351, 520], 'mt': 5, 'cut': True},
    }

    # layoutlist = {
    #     1: {'shot': 3, 'col': 2, 'row': 3, 'reso': [720, 800]},
    #     2: {'shot': 4, 'col': 2, 'row': 4, 'reso': [540, 800]},
    #     3: {'shot': 4, 'col': 2, 'row': 2, 'reso': [1080, 800]},
    # }

    def set_frame(self, type):
        self.frametype = type

    def set_layout(self, type):
        self.layout.clear()
        self.layout.update(self.layoutlist[type])

    def get_frame(self):
        return self.frametype

    def get_layout(self):
        return self.layout


class FrameSelect_1(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("Frame")

        btntomain = tk.Button(self, text="Main Menu", command=self.to_main)
        btntomain.grid(row=5, column=1, pady=20)
        self.btntowait = tk.Button(self, text="continue", command=lambda: self.to_wait())

        bFrame1 = tk.Button(self, text="frame 1", command=lambda: self.click_frame(3))
        bFrame2 = tk.Button(self, text="frame 2", command=lambda: self.click_frame(3))
        bFrame3 = tk.Button(self, text="frame 3", command=lambda: self.click_frame(2))
        bFrame4 = tk.Button(self, text="frame 4", command=lambda: self.click_frame(2))
        bFrame1.grid(row=0, column=0)
        bFrame2.grid(row=0, column=2)
        bFrame3.grid(row=1, column=0)
        bFrame4.grid(row=1, column=2)

    def click_frame(self, fid):
        self.imgFrames = fid
        frametype = FrameType()
        frametype.set_layout(fid)
        self.btntowait.grid(row=4, column=1, pady=20)

    def to_wait(self):
        self.destroy()
        subFrame = WaitingPage(self.original_frame)

    def to_main(self):
        self.destroy()
        self.original_frame.show()


class FrameSelect_2(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("Frame")

        btntomain = tk.Button(self, text="Main Menu", command=self.to_main)
        btntomain.grid(row=5, column=1, pady=20)
        self.btntowait = tk.Button(self, text="continue", command=lambda: self.to_wait())

        bFrame1 = tk.Button(self, text="frame 1", command=lambda: self.click_frame(0))
        bFrame2 = tk.Button(self, text="frame 2", command=lambda: self.click_frame(0))
        bFrame3 = tk.Button(self, text="frame 3", command=lambda: self.click_frame(1))
        bFrame4 = tk.Button(self, text="frame 4", command=lambda: self.click_frame(1))
        bFrame1.grid(row=0, column=0)
        bFrame2.grid(row=0, column=2)
        bFrame3.grid(row=1, column=0)
        bFrame4.grid(row=1, column=2)

    def click_frame(self, fid):
        self.imgFrames = fid
        frametype = FrameType()
        frametype.set_layout(fid)
        self.btntowait.grid(row=4, column=1, pady=20)

    def to_wait(self):
        self.destroy()
        subFrame = WaitingPage(self.original_frame)

    def to_main(self):
        self.destroy()
        self.original_frame.show()


class FrameSelect_3(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("Frame")

        btntomain = tk.Button(self, text="Main Menu", command=self.to_main)
        btntomain.grid(row=5, column=2, pady=20)
        self.btntowait = tk.Button(self, text="continue", command=lambda: self.to_wait())

        bFrame1 = tk.Button(self, text="frame 1", command=lambda: self.click_frame(5))
        bFrame2 = tk.Button(self, text="frame 2", command=lambda: self.click_frame(5))
        bFrame3 = tk.Button(self, text="frame 3", command=lambda: self.click_frame(6))
        bFrame4 = tk.Button(self, text="frame 4", command=lambda: self.click_frame(6))
        bFrame5 = tk.Button(self, text="frame 5", command=lambda: self.click_frame(2))
        bFrame6 = tk.Button(self, text="frame 6", command=lambda: self.click_frame(2))
        bFrame7 = tk.Button(self, text="frame 7", command=lambda: self.click_frame(2))
        bFrame1.grid(row=0, column=1)
        bFrame2.grid(row=0, column=3)
        bFrame3.grid(row=1, column=1)
        bFrame4.grid(row=1, column=3)
        bFrame5.grid(row=2, column=0)
        bFrame6.grid(row=2, column=2)
        bFrame7.grid(row=2, column=4)

    def click_frame(self, fid):
        self.imgFrames = fid
        frametype = FrameType()
        frametype.set_layout(fid)
        self.btntowait.grid(row=4, column=2, pady=20)

    def to_wait(self):
        self.destroy()
        subFrame = WaitingPage(self.original_frame)

    def to_main(self):
        self.destroy()
        self.original_frame.show()


class FrameSelect_4(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("Frame")

        btntomain = tk.Button(self, text="Main Menu", command=self.to_main)
        btntomain.grid(row=5, column=2, pady=20)
        self.btntowait = tk.Button(self, text="continue", command=lambda: self.to_wait())

        bFrame1 = tk.Button(self, text="frame 1", command=lambda: self.click_frame(3))
        bFrame2 = tk.Button(self, text="frame 2", command=lambda: self.click_frame(3))
        bFrame3 = tk.Button(self, text="frame 3", command=lambda: self.click_frame(4))
        bFrame4 = tk.Button(self, text="frame 4", command=lambda: self.click_frame(4))
        bFrame5 = tk.Button(self, text="frame 5", command=lambda: self.click_frame(2))
        bFrame6 = tk.Button(self, text="frame 6", command=lambda: self.click_frame(2))
        bFrame7 = tk.Button(self, text="frame 7", command=lambda: self.click_frame(2))
        bFrame1.grid(row=0, column=1)
        bFrame2.grid(row=0, column=3)
        bFrame3.grid(row=1, column=1)
        bFrame4.grid(row=1, column=3)
        bFrame5.grid(row=2, column=0)
        bFrame6.grid(row=2, column=2)
        bFrame7.grid(row=2, column=4)

    def click_frame(self, fid):
        self.imgFrames = fid
        frametype = FrameType()
        frametype.set_layout(fid)
        self.btntowait.grid(row=4, column=2, pady=20)

    def to_wait(self):
        self.destroy()
        subFrame = WaitingPage(self.original_frame)

    def to_main(self):
        self.destroy()
        self.original_frame.show()


class WaitingPage(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("wait")

        # btn = tk.Button(self, text="Main Menu", command=self.to_main)
        # btn.pack()
        btn = tk.Button(self, text="GO!", command=self.to_snap)
        btn.pack()

        # initialize the root window and image panel
        self.panel = None

    def to_main(self):
        self.destroy()
        self.original_frame.show()

    def to_snap(self):
        self.destroy()
        subFrame = Snapshot(self.original_frame)


class Snapshot(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("booth")
        """ Initialize application which uses OpenCV + Tkinter. It displays
            a video stream in a Tkinter window and stores current snapshot on disk """
        self.vs = cv2.VideoCapture(0)  # capture video frames, 0 is your default video camera
        time.sleep(1)
        self.output_path = './image'  # store output path
        self.current_image = None  # current image from the camera

        # self.destructor function gets fired when the window is closed

        self.panel = tk.Label(self)  # initialize image panel
        self.panel.pack(padx=10, pady=10)

        # timer & label
        self.label1 = tk.Label(self, text='', width=10, font=('Arial', 45))
        self.label1.pack(padx=10)
        self.remaining = 0
        self.delay = config['camera']['delay']
        self.imnum = 1

        frametype = FrameType()
        self.frameleft = frametype.get_layout()['shot']
        # print(frametype.get_img_num())
        self.reso = frametype.get_layout()['reso']
        self.loopSnap(self.delay)

        # create a button, that when pressed, will take the current frame and save it to file
        # btn = tk.Button(self, text="Snapshot!", command=self.snap)
        # btn.pack(fill="x", expand=True, padx=10, pady=10)

        # start a self.video_loop that constantly pools the video sensor
        # for the most recently read frame
        self.video_loop()

    def loopSnap(self, remaining=None):
        if remaining is not None:
            self.remaining = remaining

        if self.remaining <= 0:
            self.label1.configure(text="snap")
            print('frame left before snap : {}'.format(self.frameleft))

            # if self.frameleft > 0:
            self.snap()
            # time.sleep(2)
            self.frameleft -= 1
            self.imnum += 1
            print('frame left after snap : {}'.format(self.frameleft))
            if self.frameleft > 0:
                self.loopSnap(self.delay)
            else:
                # print('finished in')
                self.to_loading()
            # else:
            #     print('finished')
            #     # to process image

        else:
            self.label1.configure(text="%d" % self.remaining)
            self.remaining = self.remaining - 1
            self.after(1000, self.loopSnap)

    def snap(self):
        """ Take snapshot and save it to the file """
        ts = datetime.datetime.now()  # grab the current timestamp
        filename = "{} image{}.png".format(ts.strftime("%Y-%m-%d_%H-%M-%S"), self.imnum)  # construct filename
        p = os.path.join(self.output_path, filename)  # construct output path
        self.current_image.save(p, "png")  # save image as jpeg file
        print("[INFO] saved {}".format(filename))

    def video_loop(self):
        """ Get frame from the video stream and show it in Tkinter """
        ok, frame = self.vs.read()  # read frame from video stream
        if ok:  # frame captured without any errors
            # cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)  # convert colors from BGR to RGBA
            # cv2image = imutils.resize(cv2image, width=1080)  # resizing
            # self.current_image = Image.fromarray(cv2image)  # convert image for PIL
            # imgtk = ImageTk.PhotoImage(image=self.current_image)  # convert image for tkinter
            # self.panel.imgtk = imgtk  # anchor imgtk so it does not be deleted by garbage-collector
            # self.panel.config(image=imgtk)  # show the image

            cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            self.current_image = Image.fromarray(cv2image)  # for save
            # imgtoshow = imutils.resize(cv2image, width=700)
            # crop and show
            imgtoshow = self.crop_img(cv2image, self.reso)  # for show
            imgtoshow = Image.fromarray(imgtoshow)
            scale_size = 0.5  # for display only
            imgtoshow = imgtoshow.resize((int(self.reso[1] * scale_size), int(self.reso[0] * scale_size)))
            imgtk = ImageTk.PhotoImage(image=imgtoshow)
            self.panel.imgtk = imgtk
            self.panel.config(image=imgtk)

        self.after(40, self.video_loop)  # call the same function after 40 milliseconds

    def destructor(self):
        """ Destroy the root object and release all resources """
        print("[INFO] closing...")
        self.vs.release()  # release web camera
        cv2.destroyAllWindows()  # it is not mandatory in this application

    def crop_img(self, img, reso):
        height = img.shape[0]
        width = img.shape[1]

        fhei = reso[0]
        fwid = reso[1]

        margin_y = (height - fhei)
        margin_x = (width - fwid)

        start_y = int(margin_y / 2)
        start_x = int(margin_x / 2)

        end_y = start_y + fhei
        end_x = start_x + fwid

        rimg = img[start_y:end_y, start_x:end_x, :]
        return rimg

    def to_loading(self):
        self.destructor()
        self.destroy()
        subFrame = Loading(self.original_frame)


class Loading(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("processing")

        btn = tk.Button(self, text="Main Menu", command=self.to_main)
        # btn.pack()

        self.imgpath = './image/'
        self.blankpath = './blank/'
        self.framepath = './frame/'

        frametype = FrameType()
        self.layout = frametype.get_layout()
        print('layout : {}'.format(self.layout))

        # {'shot': 3, 'col': 2, 'row': 3, 'reso': [468, 520]}
        self.shotamount = self.layout['shot']
        self.replaceimg()

    def createBlank(self):
        W = 1800
        H = 1200
        blankIMG = np.zeros([H, W, 1], np.uint8)
        blankIMG.fill(255)
        cv2.imwrite('./blank/blank.png', blankIMG)

    def prepareimg(self, img, reso):
        # crop
        height = img.shape[0]
        width = img.shape[1]

        fhei = reso[0]
        fwid = reso[1]

        margin_y = (height - fhei)
        margin_x = (width - fwid)

        start_y = int(margin_y / 2)
        start_x = int(margin_x / 2)

        end_y = start_y + fhei
        end_x = start_x + fwid

        rimg = img[start_y:end_y, start_x:end_x, :]
        return rimg

    def replaceimg(self):
        bimg = cv2.imread(self.blankpath + 'white.png')

        listimg = os.listdir(self.imgpath)
        listimg.sort()
        if self.shotamount == 3:
            listimg = listimg[-3:]
        else:
            listimg = listimg[-4:]

        # copy of blank white image
        fnimg = bimg.copy()
        fnimg = imutils.resize(fnimg, height=1560)  # H1560 W1040 for 720p camera
        # fnimg = imutils.resize(fnimg, height=2400)  # H2400 W1600 for 1080p camera
        for i in range(len(listimg)):
            listimg[i] = './image/' + listimg[i]

        imgobj = dict()
        indexnum = 1  # naming

        reso = self.layout['reso']

        for i in range(self.shotamount):
            kv = {indexnum: self.prepareimg(cv2.imread(listimg[i]), reso)}
            imgobj.update(kv)
            indexnum += 1
        # print(imgobj)
        # blank reso = H1560 W1040
        # margin 156 top = 78, bot = 78
        margin = int(fnimg.shape[0] * 0.05)

        start_y = margin
        start_x = 0

        px_y = self.layout['reso'][0]
        px_x = self.layout['reso'][1]

        if self.layout['row'] == 2:
            for row in range(self.layout['row']):
                fnimg[start_y:start_y + px_y, start_x:start_x + px_x, :] = imgobj[row + 1][0:reso[0], 0:reso[1], :]
                start_y = start_y + px_y
            start_y = 78
            start_x = start_x + px_x
            for col in range(self.layout['col']):
                fnimg[start_y:start_y + px_y, start_x:start_x + px_x, :] = imgobj[col + 3][0:reso[0], 0:reso[1], :]
                start_y = start_y + px_y
        else:
            for col in range(self.layout['col']):
                for row in range(self.layout['row']):
                    fnimg[start_y:start_y + px_y, start_x:start_x + px_x, :] = imgobj[row + 1][0:reso[0], 0:reso[1], :]
                    start_y = start_y + px_y
                start_y = 78
                start_x = start_x + px_x

        cv2.imwrite('output/test.jpg', fnimg)
        time.sleep(2)

        frame_to_place = self.frameResize('frame1')
        self.placeFrame(frame_to_place)

    def placeFrame(self, frame_to_place):
        print('place frame')
        # place frame_to_place on top of every image
        # print to printer with cut operation

    def frameResize(self, org_frame):
        print('resizing frame')
        # resize org_frame to match shot reso
        # return frame

    def to_main(self):
        self.destroy()
        self.original_frame.show()


########################################################################
class MyApp:
    def __init__(self, parent):
        self.root = parent
        self.root.title("FOTO")
        self.frame = tk.Frame(parent)
        self.frame.pack()

        button1 = tk.Button(self.frame, text='Promptpay', width=40, height=2, command=self.open_payment)
        button1.pack(padx=20, pady=(100, 10))
        button2 = tk.Button(self.frame, text='Coupon', width=40, height=2, command=self.open_coupon)
        button2.pack(padx=20, pady=(100, 10))

        button3 = tk.Button(self.frame, text='bypass', width=40, height=2, command=self.bypass)
        button3.pack(padx=20, pady=(100, 10))

        # self.current_image = Image.fromarray(cv2image)  # convert image for PIL
        # imgtk = ImageTk.PhotoImage(image=self.current_image)  # convert image for tkinter
        # self.panel.imgtk = imgtk  # anchor imgtk so it does not be deleted by garbage-collector
        # self.panel.config(image=imgtk)  # show the image

    def hide(self):
        self.root.withdraw()

    def open_payment(self):
        self.hide()
        subFrame = Payment(self)

    def open_coupon(self):
        self.hide()
        # subFrame = Coupon(self)
        subFrame = Coupon(self)

    def bypass(self):
        self.hide()
        # subFrame = Loading(self)

        if config.get('screen').get('frame_screen') == 1:
            subFrame = FrameSelect_1(self)
        elif config.get('screen').get('frame_screen') == 2:
            subFrame = FrameSelect_2(self)
        elif config.get('screen').get('frame_screen') == 3:
            subFrame = FrameSelect_3(self)
        elif config.get('screen').get('frame_screen') == 4:
            subFrame = FrameSelect_4(self)
        else:
            subFrame = FrameSelect_1(self)

    def show(self):
        self.root.update()
        self.root.deiconify()


if __name__ == "__main__":
    root = tk.Tk()
    config = dict()
    with open("config.json") as config_file:
        config = json.load(config_file)
    config_file.close()

    root.geometry(config.get('screen').get('screen_resolution'))
    root.attributes('-fullscreen', config.get('screen').get('full_screen'))

    app = MyApp(root)
    root.mainloop()
