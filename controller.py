from mainwindow import *
from payment import *
from frame import *
from webdialog import *
# from webviewtest import *


class Dialog_Mainwindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)


class Dialog_payment(QtWidgets.QWidget, Ui_payment):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.setupUi(self)


class Dialog_frame(QtWidgets.QWidget, Ui_frameselect):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.setupUi(self)


class Dialog_webdialog(QtWidgets.QDialog, Ui_WebDialog02):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.setupUi(self)


# class Dialog_webview(QtWidgets.QWidget, Ui_webview):
#     def __init__(self, parent=None):
#         QtWidgets.QWidget.__init__(self, parent)
        # self.setupUi(self)
