import math
import tkinter as tk
from tkinter import messagebox as msgbox
import time

import cv2
import webview
import requests
import hashlib
import datetime
import os
import json

import threading
# import cv2
from cv2 import cv2
import numpy as np
# import PIL
from PIL import Image, ImageTk
import imutils

import win32ui
import win32print
from PIL import ImageWin
import qrcode


########################################################################
class Payment(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("QR Payment")

        btn01 = tk.Button(self, text="Main Menu", command=self.to_main)
        btn02 = tk.Button(self, text="Frame", command=self.open_webview)
        btn01.pack()
        btn02.pack(pady=40)

        self.QRwindow = webview.create_window(url='', title='QR payment', width=800, height=700,
                                              on_top=True, resizable=False, frameless=True)
        self.url_makepayment = 'https://sandbox-appsrv2.chillpay.co/api/v2/Payment/'
        self.url_checkstatus = 'https://sandbox-appsrv2.chillpay.co/api/v2/PaymentStatus/'
        self.cpSKEY = 'tqtBZkzw1GHJvLVe3FPdoWNqAwe3a3369IYMYMYuD6z4dKzs36VawdBye6IzI7d5rcn6OGdOqnQN3gNlfmyjA9WWMJzYrTItJ8uR416BRWvYb8WXUmqAsBymt1QJdCK3EqG6QDo6YVOCEiWpsL0ZnzqkxI3GAT1Ty4opx'
        self.APIKey = 'HFDFNVSgdMuLYdL2Kt8ZE6DfZTcLHPLwY5wD9QLp2xRjxnAOuglP6VVa5gQcnLWo'
        self.payload_headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        self.payload_payment = {
            'MerchantCode': 'M032829',
            'OrderNo': '00001',
            'CustomerId': '001',
            'Amount': '20000',
            'ChannelCode': 'bank_qrcode',
            'Currency': '764',
            'RouteNo': '1',
            'IPAddress': '127.0.0.1',
            'APIKey': 'HFDFNVSgdMuLYdL2Kt8ZE6DfZTcLHPLwY5wD9QLp2xRjxnAOuglP6VVa5gQcnLWo',
            'Checksum': '',
        }
        self.payload_result = dict()
        self.payload_paymentstatus = {
            'MerchantCode': 'M032829',
            'TransactionId': '',
            'APIKey': 'HFDFNVSgdMuLYdL2Kt8ZE6DfZTcLHPLwY5wD9QLp2xRjxnAOuglP6VVa5gQcnLWo',
            'Checksum': '',
        }
        self.payment_makepayment()

    def payment_makepayment(self):
        # prepare payload
        s2h = self.prepares2h(self.payload_payment)
        hhStr = self.hashStr(s2h)
        self.payload_payment.update(Checksum=hhStr)

        # requests payment
        res = requests.post(url=self.url_makepayment, data=self.payload_payment, headers=self.payload_headers)
        data = res.json()

        if data.get('Status') == 0:
            self.payload_result.update(data)
            self.payload_paymentstatus.update(TransactionId=data.get('TransactionId'))
            # prepare payload status
            s2h = self.prepares2h(self.payload_paymentstatus)
            hhStr = self.hashStr(s2h)
            self.payload_paymentstatus.update(Checksum=hhStr)
            # start webview
            webview.start(self.webview_handle, self.QRwindow)
        else:
            msgbox.showerror(title='FAILED', message='make requests fail')
            self.to_main()
        # self.QRwindow.load_url(self.payload_result.get('PaymentUrl'))
        # print(self.payload_payment)

    def webview_handle(self, QRwindow):
        print('url = ', self.payload_result.get('PaymentUrl'))
        QRwindow.load_url(self.payload_result.get('PaymentUrl'))

        keepcall = True
        while keepcall:
            time.sleep(0.5)
            resStat = self.payment_status_keepcalling()
            print('loop got status ', resStat)
            if resStat == 0:
                keepcall = False
                print('FINISHED')
                self.QRwindow.destroy()
                self.to_frame()
            elif resStat in [1, 2, 3]:
                keepcall = False
                self.QRwindow.destroy()
                msgbox.showerror(title='ERROR', message='Payment Failed.')
                self.to_main()

    def payment_status_keepcalling(self):
        res = requests.post(url=self.url_checkstatus, data=self.payload_paymentstatus, headers=self.payload_headers)
        resRaw = res.json()
        resStat = resRaw.get('PaymentStatus')
        print('keepcall return status', resStat)
        return resStat

    def prepares2h(self, data):
        s2h = ''
        for i in data:
            s2h += str(data.get(i))
        s2h += self.cpSKEY
        return s2h

    def hashStr(self, s2h):
        hhStr = hashlib.md5(str(s2h).encode('utf-8')).hexdigest()
        return hhStr

    def to_main(self):
        self.destroy()
        self.original_frame.show()

    def to_frame(self):
        self.destroy()
        if config.get('screen').get('frame_screen') == 1:
            subFrame = FrameSelect_1(self.original_frame)
        elif config.get('screen').get('frame_screen') == 2:
            subFrame = FrameSelect_2(self.original_frame)
        elif config.get('screen').get('frame_screen') == 3:
            subFrame = FrameSelect_3(self.original_frame)
        elif config.get('screen').get('frame_screen') == 4:
            subFrame = FrameSelect_4(self.original_frame)
        else:
            subFrame = FrameSelect_1(self.original_frame)

    def open_webview(self):
        webview.start()

    # def close_webview(self, QRwindow):


########### demo
# class Demo(tk.Toplevel):
#     def __init__(self, original):
#         self.original_frame = original
#         tk.Toplevel.__init__(self)
#         self.geometry("400x711")
#         self.title("Coupon")
#
#         btn = tk.Button(self, text="Main Menu", command=self.to_main)
#         btn.pack()
#
#     def to_main(self):
#         self.destroy()
#         self.original_frame.show()
############

class Coupon(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("Coupon")

        # self.vs = cv2.VideoCapture(1)
        self.vs = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        self.qrCodeDetector = cv2.QRCodeDetector()
        self.current_image = None
        self.img_scan = None

        self.panel = tk.Label(self)
        self.panel.pack(padx=10, pady=10)

        btn = tk.Button(self, text="Main Menu", command=self.to_main)
        btn.pack()

        self.couponcode = ''

        self.video_loop()
        self.qrreader()

    def video_loop(self):
        ok, frame = self.vs.read()
        if ok:
            cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            cv2image = imutils.resize(cv2image, width=600)
            self.current_image = Image.fromarray(cv2image)
            self.img_scan = frame
            imgtk = ImageTk.PhotoImage(image=self.current_image)
            self.panel.imgtk = imgtk
            self.panel.config(image=imgtk)
        self.after(40, self.video_loop)

    def destructor(self):
        # print("[INFO] qr reader closing...")
        self.vs.release()
        cv2.destroyAllWindows()

    def qrreader(self):
        data, points, _ = self.qrCodeDetector.detectAndDecode(self.img_scan)
        print('scanning')
        if data:
            print('QR founded')
            self.couponcode = data
            self.check_coupon()
        else:
            self.after(1000, self.qrreader)

    def check_coupon(self):
        self.vs.release()
        # request
        payload = json.dumps({'code': self.couponcode})
        # print(payload)
        # res = requests.post(url='', data=payload)
        if self.couponcode == 'this is test code':
            self.to_frame()
        else:
            msgbox.showerror(title='FAILED', message='QR code invalid!')
            self.to_main()
        # if success : to_frame

    def to_main(self):
        self.destroy()
        self.original_frame.show()

    def to_frame(self):
        self.destructor()
        self.destroy()
        # subFrame = FrameSelect_1(self.original_frame)
        if config.get('screen').get('frame_screen') == 1:
            subFrame = FrameSelect_1(self.original_frame)
        elif config.get('screen').get('frame_screen') == 2:
            subFrame = FrameSelect_2(self.original_frame)
        elif config.get('screen').get('frame_screen') == 3:
            subFrame = FrameSelect_3(self.original_frame)
        elif config.get('screen').get('frame_screen') == 4:
            subFrame = FrameSelect_4(self.original_frame)
        else:
            subFrame = FrameSelect_1(self.original_frame)


# singleton for frametype
def singleton(cls, *args, **kw):
    instances = {}

    def _singleton(*args, **kw):
        if cls not in instances:
            instances[cls] = cls(*args, **kw)
        return instances[cls]

    return _singleton


@singleton
class FrameType:
    frametype = 0
    img_num = 0

    layout = dict()

    # layoutlist = {
    #     0: {'shot': 4, 'col': 2, 'row': 2, 'reso': [520, 520], 'mt': 0, 'mode': 'n', 'cut': True},
    #     1: {'shot': 4, 'col': 2, 'row': 2, 'reso': [520, 520], 'mt': 0, 'mode': 'n', 'cut': False},
    #     2: {'shot': 4, 'col': 2, 'row': 2, 'reso': [702, 520], 'mt': 5, 'mode': 'n', 'cut': False},
    #     3: {'shot': 3, 'col': 2, 'row': 3, 'reso': [468, 520], 'mt': 5, 'mode': 'd', 'cut': True},
    #     4: {'shot': 6, 'col': 2, 'row': 3, 'reso': [468, 520], 'mt': 5, 'mode': 'n', 'cut': True},
    #     5: {'shot': 4, 'col': 2, 'row': 4, 'reso': [351, 520], 'mt': 5, 'mode': 'd', 'cut': True},
    #     6: {'shot': 8, 'col': 2, 'row': 4, 'reso': [351, 520], 'mt': 5, 'mode': 'n', 'cut': True},
    # }

    layoutlist = {
        0: {'shot': 4, 'col': 2, 'row': 2, 'reso': [800, 800], 'mt': 0, 'mode': 'n', 'cut': True},
        1: {'shot': 4, 'col': 2, 'row': 2, 'reso': [800, 800], 'mt': 0, 'mode': 'n', 'cut': False},
        2: {'shot': 4, 'col': 2, 'row': 2, 'reso': [1080, 800], 'mt': 5, 'mode': 'n', 'cut': False},
        3: {'shot': 3, 'col': 2, 'row': 3, 'reso': [720, 800], 'mt': 5, 'mode': 'd', 'cut': True},
        4: {'shot': 6, 'col': 2, 'row': 3, 'reso': [720, 800], 'mt': 5, 'mode': 'n', 'cut': True},
        5: {'shot': 4, 'col': 2, 'row': 4, 'reso': [540, 800], 'mt': 5, 'mode': 'd', 'cut': True},
        6: {'shot': 8, 'col': 2, 'row': 4, 'reso': [540, 800], 'mt': 5, 'mode': 'n', 'cut': True},
    }

    # layoutlist = {
    #     1: {'shot': 3, 'col': 2, 'row': 3, 'reso': [720, 800]},
    #     2: {'shot': 4, 'col': 2, 'row': 4, 'reso': [540, 800]},
    #     3: {'shot': 4, 'col': 2, 'row': 2, 'reso': [1080, 800]},
    # }

    def set_frame(self, type):
        self.frametype = type

    def set_layout(self, type):
        self.layout.clear()
        self.layout.update(self.layoutlist[type])

    def get_frame(self):
        return self.frametype

    def get_layout(self):
        return self.layout


class FrameSelect_1(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("Frame")

        btntomain = tk.Button(self, text="Main Menu", command=self.to_main)
        btntomain.grid(row=5, column=1, pady=20)
        self.btntowait = tk.Button(self, text="continue", command=lambda: self.to_wait())

        bFrame1 = tk.Button(self, text="frame 1", command=lambda: self.click_frame(3))
        bFrame2 = tk.Button(self, text="frame 2", command=lambda: self.click_frame(3))
        bFrame3 = tk.Button(self, text="frame 3", command=lambda: self.click_frame(2))
        bFrame4 = tk.Button(self, text="frame 4", command=lambda: self.click_frame(2))
        bFrame1.grid(row=0, column=0)
        bFrame2.grid(row=0, column=2)
        bFrame3.grid(row=1, column=0)
        bFrame4.grid(row=1, column=2)

    def click_frame(self, fid):
        self.imgFrames = fid
        frametype = FrameType()
        frametype.set_layout(fid)
        self.btntowait.grid(row=4, column=1, pady=20)

    def to_wait(self):
        self.destroy()
        subFrame = WaitingPage(self.original_frame)

    def to_main(self):
        self.destroy()
        self.original_frame.show()


class FrameSelect_2(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("Frame")

        btntomain = tk.Button(self, text="Main Menu", command=self.to_main)
        btntomain.grid(row=5, column=1, pady=20)
        self.btntowait = tk.Button(self, text="continue", command=lambda: self.to_wait())

        bFrame1 = tk.Button(self, text="frame 1", command=lambda: self.click_frame(0))
        bFrame2 = tk.Button(self, text="frame 2", command=lambda: self.click_frame(0))
        bFrame3 = tk.Button(self, text="frame 3", command=lambda: self.click_frame(1))
        bFrame4 = tk.Button(self, text="frame 4", command=lambda: self.click_frame(1))
        bFrame1.grid(row=0, column=0)
        bFrame2.grid(row=0, column=2)
        bFrame3.grid(row=1, column=0)
        bFrame4.grid(row=1, column=2)

    def click_frame(self, fid):
        self.imgFrames = fid
        frametype = FrameType()
        frametype.set_layout(fid)
        self.btntowait.grid(row=4, column=1, pady=20)

    def to_wait(self):
        self.destroy()
        subFrame = WaitingPage(self.original_frame)

    def to_main(self):
        self.destroy()
        self.original_frame.show()


class FrameSelect_3(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("Frame")

        btntomain = tk.Button(self, text="Main Menu", command=self.to_main)
        btntomain.grid(row=5, column=2, pady=20)
        self.btntowait = tk.Button(self, text="continue", command=lambda: self.to_wait())

        bFrame1 = tk.Button(self, text="frame 1", command=lambda: self.click_frame(5))
        bFrame2 = tk.Button(self, text="frame 2", command=lambda: self.click_frame(5))
        bFrame3 = tk.Button(self, text="frame 3", command=lambda: self.click_frame(6))
        bFrame4 = tk.Button(self, text="frame 4", command=lambda: self.click_frame(6))
        bFrame5 = tk.Button(self, text="frame 5", command=lambda: self.click_frame(2))
        bFrame6 = tk.Button(self, text="frame 6", command=lambda: self.click_frame(2))
        bFrame7 = tk.Button(self, text="frame 7", command=lambda: self.click_frame(2))
        bFrame1.grid(row=0, column=1)
        bFrame2.grid(row=0, column=3)
        bFrame3.grid(row=1, column=1)
        bFrame4.grid(row=1, column=3)
        bFrame5.grid(row=2, column=0)
        bFrame6.grid(row=2, column=2)
        bFrame7.grid(row=2, column=4)

    def click_frame(self, fid):
        self.imgFrames = fid
        frametype = FrameType()
        frametype.set_layout(fid)
        self.btntowait.grid(row=4, column=2, pady=20)

    def to_wait(self):
        self.destroy()
        subFrame = WaitingPage(self.original_frame)

    def to_main(self):
        self.destroy()
        self.original_frame.show()


class FrameSelect_4(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("Frame")

        btntomain = tk.Button(self, text="Main Menu", command=self.to_main)
        btntomain.grid(row=5, column=2, pady=20)
        self.btntowait = tk.Button(self, text="continue", command=lambda: self.to_wait())

        bFrame1 = tk.Button(self, text="frame 1", command=lambda: self.click_frame(3))
        bFrame2 = tk.Button(self, text="frame 2", command=lambda: self.click_frame(3))
        bFrame3 = tk.Button(self, text="frame 3", command=lambda: self.click_frame(4))
        bFrame4 = tk.Button(self, text="frame 4", command=lambda: self.click_frame(4))
        bFrame5 = tk.Button(self, text="frame 5", command=lambda: self.click_frame(2))
        bFrame6 = tk.Button(self, text="frame 6", command=lambda: self.click_frame(2))
        bFrame7 = tk.Button(self, text="frame 7", command=lambda: self.click_frame(2))
        bFrame1.grid(row=0, column=1)
        bFrame2.grid(row=0, column=3)
        bFrame3.grid(row=1, column=1)
        bFrame4.grid(row=1, column=3)
        bFrame5.grid(row=2, column=0)
        bFrame6.grid(row=2, column=2)
        bFrame7.grid(row=2, column=4)

    def click_frame(self, fid):
        self.imgFrames = fid
        frametype = FrameType()
        frametype.set_layout(fid)
        self.btntowait.grid(row=4, column=2, pady=20)

    def to_wait(self):
        self.destroy()
        subFrame = WaitingPage(self.original_frame)

    def to_main(self):
        self.destroy()
        self.original_frame.show()


class WaitingPage(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("wait")

        # btn = tk.Button(self, text="Main Menu", command=self.to_main)
        # btn.pack()
        btn = tk.Button(self, text="GO!", command=self.to_snap)
        btn.pack()

        # initialize the root window and image panel
        self.panel = None

    def to_main(self):
        self.destroy()
        self.original_frame.show()

    def to_snap(self):
        self.destroy()
        subFrame = Snapshot(self.original_frame)


class Snapshot(tk.Toplevel):
    def __init__(self, original):
        print('init snapshot')
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("booth")
        """ Initialize application which uses OpenCV + Tkinter. It displays
            a video stream in a Tkinter window and stores current snapshot on disk """

        print('before open capread')
        self.vs = cv2.VideoCapture(0, cv2.CAP_DSHOW)  # capture video frames, 0 is your default video camera
        self.vs.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        self.vs.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

        self.output_path = './image'  # store output path
        self.current_image = None  # current image from the camera

        whiteim = cv2.cvtColor(cv2.imread('./background/whitebg.png'), cv2.COLOR_BGR2RGBA)
        # whiteim = imutils.resize(whiteim, height=400)
        whiteim = Image.fromarray(whiteim)
        self.whiteim = whiteim.resize((520, 468))
        white = ImageTk.PhotoImage(image=self.whiteim)

        self.panel = tk.Label(self)  # initialize image panel
        self.panel.pack(padx=10, pady=10)

        self.white_panel = tk.Label(self)
        self.white_panel.whiteim = white
        self.white_panel.config(image=white)
        # self.white_panel.place(anchor='nw', x=20, y=10, width=350, height=480)

        # timer & label
        self.label1 = tk.Label(self, text='', width=10, font=('Arial', 45), background='white')
        self.label1.pack(padx=10)
        self.remaining = 0
        self.delay = config['camera']['delay']
        self.imnum = 1

        frametype = FrameType()
        self.frameleft = frametype.get_layout()['shot']
        self.reso = frametype.get_layout()['reso']

        self.video_loop()
        self.loopSnap(self.delay)

    def white_change(self):
        print('white change')
        self.white_panel.place(anchor='nw', x=20, y=10, width=360, height=480)
        # white = ImageTk.PhotoImage(image=self.whiteim)
        # self.panel.white = white
        # self.panel.config(image=white)
        print('after white change')

    def loopSnap(self, remaining=None):
        if remaining is not None:
            self.remaining = remaining

        if self.remaining <= 0:
            self.label1.configure(text="snap")
            print('frame left before snap : {}'.format(self.frameleft))

            self.snap()

            self.frameleft -= 1
            self.imnum += 1
            print('frame left after snap : {}'.format(self.frameleft))
            if self.frameleft > 0:
                self.after(1000, self.loopSnap(self.delay))
            else:
                self.to_loading()

        else:
            self.label1.configure(text="%d" % (self.remaining - 1))

            if self.remaining == 1:
                self.white_change()
            else:
                self.white_panel.place_forget()

            self.remaining = self.remaining - 1
            self.after(1000, self.loopSnap)

    def snap(self):
        """ Take snapshot and save it to the file """
        ts = datetime.datetime.now()  # grab the current timestamp
        filename = "{} image{}.png".format(ts.strftime("%Y-%m-%d_%H-%M-%S"), self.imnum)  # construct filename
        p = os.path.join(self.output_path, filename)  # construct output path
        self.current_image.save(p, "png")  # save image as jpeg file
        print("[INFO] saved {}".format(filename))
        # threading.current_thread().join()

    def video_loop(self):
        """ Get frame from the video stream and show it in Tkinter """
        ok, frame = self.vs.read()  # read frame from video stream
        if ok:  # frame captured without any errors
            # cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)  # convert colors from BGR to RGBA
            # cv2image = imutils.resize(cv2image, width=1080)  # resizing
            # self.current_image = Image.fromarray(cv2image)  # convert image for PIL
            # imgtk = ImageTk.PhotoImage(image=self.current_image)  # convert image for tkinter
            # self.panel.imgtk = imgtk  # anchor imgtk so it does not be deleted by garbage-collector
            # self.panel.config(image=imgtk)  # show the image

            cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            self.current_image = Image.fromarray(cv2image)  # for save

            # crop and show
            # imgtoshow = imutils.resize(cv2image, width=700)
            imgtoshow = self.crop_img(cv2image, self.reso)  # for show
            imgtoshow = Image.fromarray(imgtoshow)
            scale_size = 0.4  # for display only
            # scale_size = 1
            imgtoshow = imgtoshow.resize((int(self.reso[1] * scale_size), int(self.reso[0] * scale_size)))
            imgtk = ImageTk.PhotoImage(image=imgtoshow)
            self.panel.imgtk = imgtk
            self.panel.config(image=imgtk)

        self.after(40, self.video_loop)  # call the same function after 40 milliseconds

    def destructor(self):
        """ Destroy the root object and release all resources """
        print("[INFO] closing...")
        self.vs.release()  # release web camera
        cv2.destroyAllWindows()  # it is not mandatory in this application

    def crop_img(self, img, reso):
        height = img.shape[0]
        width = img.shape[1]

        fhei = reso[0]
        fwid = reso[1]

        margin_y = (height - fhei)
        margin_x = (width - fwid)

        start_y = int(margin_y / 2)
        start_x = int(margin_x / 2)

        end_y = start_y + fhei
        end_x = start_x + fwid

        rimg = img[start_y:end_y, start_x:end_x, :]
        return rimg

    def to_loading(self):
        self.destructor()
        self.destroy()
        subFrame = Loading(self.original_frame)


class Loading(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("processing")

        btn = tk.Button(self, text="Main Menu", command=self.to_main)
        # btn.pack()

        self.imgpath = './image/'
        self.blankpath = './blank/'
        self.framepath = './frame/'

        frametype = FrameType()
        self.layout = frametype.get_layout()
        print('layout : {}'.format(self.layout))

        # {'shot': 3, 'col': 2, 'row': 3, 'reso': [468, 520]}
        self.shotamount = self.layout['shot']

        th_replace = threading.Thread(target=self.replaceimg)
        th_replace.start()
        # self.replaceimg()

    def prepareimg(self, img, reso):
        # crop
        height = img.shape[0]
        width = img.shape[1]

        fhei = reso[0]
        fwid = reso[1]

        margin_y = (height - fhei)
        margin_x = (width - fwid)

        start_y = int(margin_y / 2)
        start_x = int(margin_x / 2)

        end_y = start_y + fhei
        end_x = start_x + fwid

        rimg = img[start_y:end_y, start_x:end_x, :]
        return rimg

    def replaceimg(self):
        bimg = cv2.imread(self.blankpath + 'white.png')

        listimg = os.listdir(self.imgpath)
        listimg.sort()
        # if self.shotamount == 3:
        #     listimg = listimg[-3:]
        # else:
        #     listimg = listimg[-4:]
        listimg = listimg[-self.shotamount:]

        # copy of blank white image
        fnimg = bimg.copy()
        # fnimg = imutils.resize(fnimg, height=1560)  # H1560 W1040 for 720p camera
        fnimg = imutils.resize(fnimg, height=2400)  # H2400 W1600 for 1080p camera
        for i in range(len(listimg)):
            listimg[i] = './image/' + listimg[i]

        imgobj = dict()
        indexnum = 1  # naming

        reso = self.layout['reso']

        for i in range(self.shotamount):
            kv = {indexnum: self.prepareimg(cv2.imread(listimg[i]), reso)}
            imgobj.update(kv)
            indexnum += 1
        # print(imgobj)
        # blank reso = H1560 W1040
        # margin 156 top = 78, bot = 78
        # if self.layout['mt'] == 5
        margin = int(fnimg.shape[0] * (self.layout['mt'] / 100))

        start_y = margin
        start_x = 0

        px_y = self.layout['reso'][0]
        px_x = self.layout['reso'][1]

        if self.layout['mode'] == 'n':
            sh = 1
            for col in range(self.layout['col']):
                for row in range(self.layout['row']):
                    fnimg[start_y:start_y + px_y, start_x:start_x + px_x, :] = imgobj[row + sh][0:reso[0], 0:reso[1], :]
                    start_y = start_y + px_y
                start_y = margin
                start_x = start_x + px_x
                sh += (self.layout['shot'] / 2)
                # for row in range(self.layout['row']):
                #     fnimg[start_y:start_y + px_y, start_x:start_x + px_x, :] = imgobj[col + sh][0:reso[0], 0:reso[1], :]
                #     start_y = start_y + px_y
        elif self.layout['mode'] == 'd':
            for col in range(self.layout['col']):
                for row in range(self.layout['row']):
                    fnimg[start_y:start_y + px_y, start_x:start_x + px_x, :] = imgobj[row + 1][0:reso[0], 0:reso[1], :]
                    start_y = start_y + px_y
                start_y = margin
                start_x = start_x + px_x

        cv2.imwrite('output/test.png', fnimg)

        output = self.placeFrame()
        cv2.imwrite('./output/test2.png', output)
        print('done')
        print(threading.current_thread())
        self.upload_img()

        # self.to_printer()

    def placeFrame(self):
        print('place frame')
        frame_to_place = cv2.imread(self.framepath + 'Frame3.png', -1)
        org_img = cv2.imread("./output/test.png")

        frame_to_place = self.frameResize(frame_to_place)

        back_image = org_img.copy()
        front_image = frame_to_place.copy()

        start_y, end_y = 0, back_image.shape[0]
        start_x, end_x = 0, back_image.shape[1]

        alpha_f = front_image[:, :, 3] / 255.0
        alpha_b = 1.0 - alpha_f

        for c in range(0, 3):
            back_image[start_y:end_y, start_x:end_x, c] = (alpha_f * front_image[:, :, c] +
                                                           alpha_b * back_image[start_y:end_y, start_x:end_x, c])

        # added_image = cv2.addWeighted(back_image, 1, front_image, 1, 0)
        print('finished')
        # return added_image
        return back_image

    def frameResize(self, org_frame):
        print('resizing frame')
        # org_frame = imutils.resize(org_frame, height=1560)  # for 1280
        org_frame = imutils.resize(org_frame, height=2400)
        return org_frame

    def upload_img(self):
        url = 'http://192.168.1.8:3049/api/upload/foto'
        image = {'image': open('./output/test2.png', 'rb')}
        res = requests.post(url, files=image)
        data = res.json()
        print(data)
        self.genQR(data['path'])

    def genQR(self, path):
        url = 'http://192.168.1.8:3049/api/image/'
        path = path
        fpath = url + path
        print(fpath)
        img = qrcode.make(fpath)
        type(img)
        img.save('./output/qr_test.png')
        self.to_showcase()

    def to_showcase(self):
        self.destroy()
        subFrame = Showcase(self.original_frame)

    def to_main(self):
        self.destroy()
        self.original_frame.show()


class Showcase(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry("400x711")
        self.title("Result")

        frametype = FrameType()
        self.layout = frametype.get_layout()

        btn = tk.Button(self, text="Main Menu", command=self.to_main)
        btn.pack()

        self.panel = tk.Label(self)
        self.panel.pack(padx=10, pady=10)

        self.qrpan = tk.Label(self)
        self.qrpan.pack(padx=10, pady=10)

        self.config_img()

    def config_img(self):
        # img = Image.open('./output/test.jpg')
        img = Image.open('./output/test2.png')
        img = img.resize((200, 300))
        imgtk = ImageTk.PhotoImage(image=img)
        self.panel.imgtk = imgtk
        self.panel.config(image=imgtk)

        qrimg = Image.open('./output/qr_test.png')
        qrimg = qrimg.resize((200, 200))
        qrimgtk = ImageTk.PhotoImage(image=qrimg)
        self.qrpan.qrimgtk = qrimgtk
        self.qrpan.config(image=qrimgtk)

    def to_printer(self):
        PRINTER_DEFAULTS = {"DesiredAccess": win32print.PRINTER_ALL_ACCESS}
        pHandle = win32print.OpenPrinter('DS-RX1', PRINTER_DEFAULTS)
        properties = win32print.GetPrinter(pHandle, 2)

        cuthalf = self.layout['cut']

        cut_2in = b'DINU"\x008\x01\x94\x03|\x00\x04Oy\xdf\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x04\x00\x01\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x008\x01\x00\x00SMTJ\x00\x00\x00\x00\x10\x00(\x01D\x00S\x00-\x00R\x00X\x001\x00\x00\x00InputBin\x00FORMSOURCE\x00RESDLL\x00UniresDLL\x00Orientation\x00LANDSCAPE_CC270\x00Resolution\x00Option1\x00PaperSize\x00PC\x00PrintMargin\x00MarginOff\x00OVERCOATTYPE\x00OPTYPE_LUSTER\x00PRINTBUFFCONTROL\x00PBC_CLEAR\x00CUTTERCONTROL\x00CUT_2INCH\x00MediaType\x00STANDARD\x00ColorMode\x0024bpp\x00Halftone\x00HT_PATSIZE_SUPERCELL_M\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00|\x00\x00\x00TFSM\x04\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00\x07\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00'
        cut_std = b'DINU"\x008\x01\x94\x03|\x00\x04Oy\xdf\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x04\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x008\x01\x00\x00SMTJ\x00\x00\x00\x00\x10\x00(\x01D\x00S\x00-\x00R\x00X\x001\x00\x00\x00InputBin\x00FORMSOURCE\x00RESDLL\x00UniresDLL\x00Orientation\x00LANDSCAPE_CC270\x00Resolution\x00Option1\x00PaperSize\x00PC\x00PrintMargin\x00MarginOff\x00OVERCOATTYPE\x00OPTYPE_LUSTER\x00PRINTBUFFCONTROL\x00PBC_CLEAR\x00CUTTERCONTROL\x00CUT_STANDARD\x00MediaType\x00STANDARD\x00ColorMode\x0024bpp\x00Halftone\x00HT_PATSIZE_SUPERCELL_M\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00|\x00\x00\x00TFSM\x04\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'

        p = properties
        if cuthalf:
            p['pDevMode'].DriverData = cut_2in
            win32print.SetPrinter(pHandle, 2, p, 0)
        else:
            p['pDevMode'].DriverData = cut_std
            win32print.SetPrinter(pHandle, 2, p, 0)

        bmp = Image.open('output/test.jpg')

        hDC = win32ui.CreateDC()
        hDC.CreatePrinterDC(win32print.GetDefaultPrinter())
        hDC.StartDoc("FOTO")
        hDC.StartPage()

        dib = ImageWin.Dib(bmp)
        x1 = 0
        y1 = 0
        x2 = 1239  # full paper 4.13" * 6.15" * 300dpi
        y2 = 1845

        dib.draw(hDC.GetHandleOutput(), (x1, y1, x2, y2))

        hDC.EndPage()
        hDC.EndDoc()
        hDC.DeleteDC()

    def upload_img(self):
        url = 'http://192.168.1.8:3049/api/upload/foto'
        image = {'image': open('./output/test2.png', 'rb')}
        res = requests.post(url, files=image)
        data = res.json()
        print(data)
        self.genQR(data['path'])

    def genQR(self, path):
        url = 'http://192.168.1.8:3049/api/image/'
        path = path
        fpath = url + path
        print(fpath)
        img = qrcode.make(fpath)
        type(img)
        img.save('./output/qr_test.png')

    def to_main(self):
        self.destroy()
        self.original_frame.show()


class ChoosePayment(tk.Toplevel):
    def __init__(self, original):
        self.original_frame = original
        tk.Toplevel.__init__(self)
        self.geometry(config['screen']['screen_resolution'])
        self.title("Choose Payment")
        self.configure(bg='black')

        self.bgcanvas = tk.Canvas(self, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.bgcanvas.pack()

        BGimg = Image.open('./background/FTMC/BG/2.jpg')
        BGimg = BGimg.resize((screen_width, screen_height), Image.ANTIALIAS)
        BGimg = ImageTk.PhotoImage(image=BGimg)
        self.bgcanvas.BGimg = BGimg
        self.bgcanvas.create_image(0, 0, image=BGimg, anchor='nw')

        btn1 = Image.open('./background/FTMC/Button/QR CODE.jpg')
        btn1 = btn1.resize((250, 125), Image.ANTIALIAS)
        btn1 = ImageTk.PhotoImage(image=btn1)
        self.bgcanvas.btn1 = btn1
        btn1action = self.bgcanvas.create_image(60, 225, image=btn1, anchor='nw')
        self.bgcanvas.tag_bind(btn1action, '<Button-1>', lambda x: self.open_payment())

        btn2 = Image.open('./background/FTMC/Button/VOUCHER.jpg')
        btn2 = btn2.resize((250, 125), Image.ANTIALIAS)
        btn2 = ImageTk.PhotoImage(image=btn2)
        self.bgcanvas.btn2 = btn2
        btn2action = self.bgcanvas.create_image(60, 425, image=btn2, anchor='nw')
        self.bgcanvas.tag_bind(btn2action, '<Button-1>', lambda y: self.open_coupon())

        # button1 = tk.Button(self, text='Promptpay', width=40, height=2, command=self.open_payment)
        # button1.pack(padx=20, pady=(100, 10))
        # button2 = tk.Button(self, text='Coupon', width=40, height=2, command=self.open_coupon)
        # button2.pack(padx=20, pady=(100, 10))
        #
        button3 = tk.Button(self, text='bypass', width=40, height=2, command=self.bypass)
        # button3.pack(padx=20, pady=(100, 10))
        button3.place(width=100, height=100)

        # btn = tk.Button(self, text="Main Menu", command=self.to_main)
        # btn.pack()

    def open_payment(self):
        self.destroy()
        subFrame = Payment(self.original_frame)

    def open_coupon(self):
        self.destroy()
        # subFrame = Coupon(self)
        subFrame = Coupon(self.original_frame)

    def bypass(self):
        self.destroy()
        # subFrame = Loading(self)

        if config.get('screen').get('frame_screen') == 1:
            subFrame = FrameSelect_1(self.original_frame)
        elif config.get('screen').get('frame_screen') == 2:
            subFrame = FrameSelect_2(self.original_frame)
        elif config.get('screen').get('frame_screen') == 3:
            subFrame = FrameSelect_3(self.original_frame)
        elif config.get('screen').get('frame_screen') == 4:
            subFrame = FrameSelect_4(self.original_frame)
        else:
            subFrame = FrameSelect_1(self.original_frame)

    def to_main(self):
        self.destroy()
        self.original_frame.show()


########################################################################
class MyApp:
    def __init__(self, parent):
        self.root = parent
        self.root.title("FOTO")
        # self.frame = tk.Frame(parent)
        # self.frame.pack()

        # BG_width, BG_height = [int(i) for i in (config['screen']['screen_resolution'].split('x'))]
        self.canvas = tk.Canvas(self.root, width=screen_width, height=screen_height, bd=0, highlightthickness=0)
        self.canvas.pack()

        BG = Image.open('./background/FTMC/BG/1.jpg')
        BG = BG.resize((screen_width, screen_height), Image.ANTIALIAS)
        BG = ImageTk.PhotoImage(image=BG)
        self.canvas.BGimg = BG
        BGaction = self.canvas.create_image(0, 0, image=BG, anchor='nw')
        self.canvas.tag_bind(BGaction, '<Button-1>', lambda x: self.choose_payment())

        # self.panel = tk.Label(self.canvas)
        # self.panel.pack(fill='both')

        # BG = Image.open('./background/FTMC/BG/1.jpg')
        # BG = BG.resize((BG_width, BG_height), Image.ANTIALIAS)
        # BG = ImageTk.PhotoImage(image=BG)
        # self.panel.BG = BG
        # self.panel.config(image=BG)

        # button1 = tk.Button(self.canvas, command=self.choose_payment, bd=0)
        # transpa = Image.open('./background/FTMC/BG/1.jpg')
        # transpa = transpa.resize((BG_width, BG_height), Image.ANTIALIAS)
        # transpa = ImageTk.PhotoImage(image=transpa)
        # button1.imgtk = transpa
        # button1.config(image=transpa)
        # button1.pack()
        # button1.place(anchor='center')

    def bypass(self, arg):
        print('bypass : {}'.format(arg))

    def hide(self):
        self.root.withdraw()

    def choose_payment(self):
        self.hide()
        subFrame = ChoosePayment(self)

    def show(self):
        self.root.update()
        self.root.deiconify()


if __name__ == "__main__":
    root = tk.Tk()
    config = dict()
    with open("config.json") as config_file:
        config = json.load(config_file)
    config_file.close()

    screen_width, screen_height = [int(i) for i in (config['screen']['screen_resolution'].split('x'))]

    root.geometry(config.get('screen').get('screen_resolution'))
    root.configure(bg='black')
    root.attributes('-fullscreen', config.get('screen').get('full_screen'))
    # root.wm_attributes("-topmost", True)
    # root.wm_attributes("-transparentcolor", "white")

    app = MyApp(root)
    root.mainloop()
